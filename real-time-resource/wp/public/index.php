<?php
namespace Fractux\App;

require_once __DIR__ . '/../vendor/autoload.php';

Server::serve( App::mount(), Server\Request::buildRequest() );
