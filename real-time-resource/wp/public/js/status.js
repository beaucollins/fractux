/**
 * @flow
 */

init(
    new Promise(resolve => document.addEventListener('DOMContentLoaded', resolve))
        .then(() => document.createElement('div'))
        .then(node => document.body ? document.body.appendChild(node) && node : Promise.reject(new Error('body not present')))
    ).then(console.log.bind(console, 'done'));

async function init(loader/*: Promise<HTMLDivElement>*/)/*: Promise<HTMLDivElement> */ {
    const node = await loader;
    console.log('node', node);
    const register = document.querySelector('[href="#register"]');
    (register && register.addEventListener('click', async (event/*: MouseEvent */) => {
        event.preventDefault();
        const result = await fetch('/system/connect', {method: 'POST', headers: {'Accept': 'application/json'}}).then(response => response.json());
        console.log('connecting?', result);
    }));

    const test = document.querySelector('[href="#test"]');
    (test && test.addEventListener('click', async (event/*: MouseEvent */) => {
        event.preventDefault();
        const result = await fetch('/system/test', {method: 'POST', headers: {'Accept': 'application/json'}}).then(response => response.json());
        console.log('test?', result);
    }));
    return node;
}

fetch( window.location, { method: 'POST', headers: { 'Accept': 'application/json', 'X-Other': 'wtf' } } )
    .then(response => response.json())
    .then(console.log.bind(console, 'response'));

window.fractux = function(url/*: string */) {
    const socket = new WebSocket( url );
    socket.addEventListener('open', () => {
        console.log('open');
        const timer = setInterval(
            () => socket.send(JSON.stringify({crap: Date.now()})),
            500
        )
        socket.addEventListener('close', () => {
            clearInterval(timer);
        });
    });
    socket.addEventListener('error', () => {
        console.log('error');
    });
    socket.addEventListener('close', (...args) => {
        console.log('close', ...args);
    });
    console.log('hi');
}
