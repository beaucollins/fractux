<?php
namespace Fractux\App;

final class Accept {

	const JSON = 'json';
	const HTML = 'html';

	/**
	 * @param IRequest $request
	 * @return 'json' | 'html'
	 */
	public static function from( $request ) {
		$acceptHeader = self::header( $request, 'accept', 'text/html' );
		$types = explode( ', ', $acceptHeader );

		foreach ( $types as $type ) {
			if ( $type === 'application/json' ) {
				return self::JSON;
			}
			if ( $type === '*/*' ) {
				return self::HTML;
			}
		}

		return self::HTML;
	}

	/**
	 * @param IRequest $request
	 * @param string $key
	 * @param string $default
	 * @return string
	 */
	static function header( $request, $key, $default ) {
		$headerKey = strtolower( $key );
		$headers = $request->getHeaders();

		foreach ( $headers as $header => $value ) {
			if ( strtolower( $header ) === $headerKey ) {
				return is_array( $value ) ? $value[0] : $value;
			}
		}

		return $default;
	}
}
