<?php
namespace Fractux\App;

use Fractux\Html\Html;
use Fractux\Status\Routes as StatusRoutes;

use function Fractux\Html\{div, safe, body, element, document, html, head, title};

/**
 * The main entrypoint for serving content. Uses `Route::combine` and `Route::mount` to
 * compose a set of endpoints to serve.
 */
final class App implements IEndpoint {

	/**
	 * The application and all if its routing.
	 *
	 * @return IEndpoint
	 */
	static function mount() {
		return new self(
			combine(
				redirectTrailingSlashes(),
				/**
				* Example of prefixing as a means of responding to a single path with different methods.
				*/
				prefix(
					'/hello', combine(
						route( getPath( '/' ), View::static( Layouts::form() ) ),
						route( postPath( '/' ), Route::handle(
							/**
							 * @param string $path
							 * @param IRequest $request
							 * @return IResponse
							 * @psalm-suppress UnusedClosureParam
							 */
							function( $path, $request ) {
								/**
								 * @var mixed
								 */
								$body = $request->getBody();

								if ( $body === null ) {
									return Route::badRequest( $request, 'No body provided' );
								}

								if ( ! is_array( $body ) || ! key_exists( 'name', $body ) ) {
									return Route::badRequest( $request, 'No name provided.' );
								}

								/**
								 * @var string
								 */
								$name = $body['name'];

								return new Redirect( '/hello/' . urlencode( $name ), 302 );
							}
						) ),
					)
				),
				prefix( '/system', StatusRoutes::mount() ),
				route( getPath( '/launch' ), View::static( div( [], '<Hello World> 🚀' ) ) ),
				route( get( Path::like( '/hello/:name' ) ), View::respond( Layouts::greet() ) ),
				prefix(
					'/fractux', combine(
						route( getPath( '/' ), View::static( Layouts::debug() ) ),
						route( get( Path::like( '/hello/:name' ) ), View::respond( Layouts::greet() ) )
					)
				),
				route(
					getPath( '/closure' ),
					Route::handle(
						/**
						 * @param string $path
						 * @param IRequest $request
						 * @return IResponse
						 * @psalm-suppress UnusedClosureParam
						 */
						function( $path, $request ) {
							return new Redirect( '/?from=' . $path, 301 );
						}
					)
				),
				route(
					MatcherMap::map(
						getPath( '/exception-handler' ),
						/**
						 * @param string $path
						 */
						function( $path ) {
							return new \RuntimeException( 'You are seeing this on purpose because you came to ' . $path );
						}
					),
					ErrorResponseHandler::handle()
				),
				route( getPath( '/' ), View::static( document(
					html( [],
						head( [], title( [], 'Fractux CRE' ) ),
						body( [ 'style' => 'margin: 20%; text-align: center; font-size: 256px' ], '⚡️' )
					)
				) ) ),
			)
		);
	}

	/**
	 * @var IMatcher<IRequest, IResponse>
	 */
	private $route;

	/**
	 * @param IMatcher<IRequest, IResponse> $route
	 */
	private function __construct( $route ) {
		$this->route = $route;
	}

	public function serve( $request ) {
		$response = $this->route->matches( $request );
		if ( $response instanceof IUnmatched ) {
			return Route::notFound( $request );
		}
		return $response;
	}
}

/**
 * @return IMatcher<IRequest,IResponse>
 */
function redirectTrailingSlashes() {
	return route(
		get(
			Path::match(
				function( $path ) {
					if ( $path === '/' ) {
						return Route::unmatched();
					}

					$last = substr( $path, strlen( $path ) - 1 );
					if ( $last !== '/' ) {
						return Route::unmatched();
					}

					$leading = substr( $path, 0, strlen( $path ) - 1 );

					if ( $leading === false ) {
						return Route::unmatched();
					}
					return $leading;
				}
			)
		),
		Route::handle(
			/**
			 * @param string $path
			 * @param IRequest $request
			 * @return IResponse
			 * @psalm-suppress UnusedClosureParam
			 */
			function( $path, $request ) {
				return new Redirect( $path, 301 );
			}
		)
	);
}
