<?php
namespace Fractux\App;
/**
 * @template T
 * @template I
 * @template O
 * @implements IHandler<T,I,O>
 */
final class ClosureHandler implements IHandler {

	/**
	 * @var closure(T,I):O
	 */
	private $handler;

	/**
	 * @param closure(T,I):O $handler
	 */
	function __construct( $handler ) {
		$this->handler = $handler;
	}

	/**
	 * @param T $param
	 * @param I $incoming
	 * @return O
	 */
	public function serve( $param, $incoming ) {
		$handler = $this->handler;
		return $handler( $param, $incoming );
	}

}
