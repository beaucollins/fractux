<?php
namespace Fractux\App;

/**
 * @template I
 * @template T
 *
 * @implements IMatcher<I,T>
 */
final class ClosureMatcher implements IMatcher {

	/**
	 * @var closure(I):(IUnmatched|T)
	 */
	private $matcher;

	/**
	 * @param closure(I):(IUnmatched|T) $matcher
	 */
	function __construct( $matcher ) {
		$this->matcher = $matcher;
	}

	public function matches( $incoming ) {
		$matcher = $this->matcher;

		return $matcher( $incoming );
	}

}
