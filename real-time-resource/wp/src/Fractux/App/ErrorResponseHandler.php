<?php
namespace Fractux\App;

use Fractux\Html\{IDomElement, Html};
use function Fractux\Html\{
	document, html, body, section, h1, p, pre,
	ol, li, ul, span, table, tbody, td, th, tr,
	title, head, meta, h2, small, em, header,
	stylesheet, fragment
};

use Throwable;
use JsonSerializable;

/**
 * Responds to Exceptions and renders a page with debugging information.
 *
 * @implements IHandler<Throwable,IRequest,IResponse>
 */
final class ErrorResponseHandler implements IHandler {

	/**
	 * Construct a new error response handler.
	 *
	 * @return IHandler<Throwable,IRequest,IResponse>
	 */
	static function handle() {
		return new self();
	}

	/**
	 * @param \Throwable $error
	 * @return \JsonSerializable
	 */
	static function json( $error ) {
		return new class( $error ) implements JsonSerializable {
			/**
			 * @var \Throwable $error;
			 */
			private $error;

			/**
			 * @param \Throwable $error
			 */
			function __construct( $error ) {
				$this->error = $error;
			}

			function jsonSerialize() {
				return [
					'type' => get_class( $this->error ),
					'message' => $this->error->getMessage(),
					'line' => $this->error->getLine(),
					'file' => $this->error->getFile(),
					'trace' => $this->error->getTrace(),
				];
			}

		};
	}

	/**
	 * Build a self-contained HTML document descrbing the error and request.
	 *
	 * @param IRequest $request
	 * @param Throwable $error
	 * @param string $graphic
	 * @return IDomElement
	 */
	static function document( $request, $error, $graphic = '💥' ) {
		return document(
			html( [],
				head( [],
					meta( [ 'name' => 'viewport', 'content' => 'width=device-width, initial-scale=1' ] ),
					stylesheet( file_data_uri( file_get_contents( __DIR__ . '/assets/style.css' ), 'text/css' ) ),
					title( [], $error->getMessage() ),
				),
				body( [],
					header( [], $graphic ),
					self::errorHeader( $error ),
					self::requestDetails( $request ),
					self::errorDetails( $error ),
				),
			)
		);
	}

	/**
	 * @param Throwable $error
	 * @return IDomElement
	 */
	static function errorHeader( $error ) {
		return (
			section( [ 'class' => 'error-reason' ],
				h1( [], [ '⚠️ ' . get_class( $error ) ] ),
				p( [], $error->getMessage() ),
				p( [ 'class' => 'reference' ],
					$error->getFile(),
					':',
					(string) $error->getLine()
				),
			)
		);
	}

	/**
	 * @param Throwable $error
	 * @return IDomElement
	 */
	static function errorDetails( $error ) {
		return (
			section( [ 'class' => 'error-details' ],
				h2( [], [ '📌 ', 'Stack Trace' ] ),
				ul(
					[],
					array_map(
						/**
						 * @param array $trace
						 * @return IDomElement
						 */
						function( $trace ) {
							return li( [], traceLine( $trace ) );
						},
						$error->getTrace(),
					)
				),
			)
		);
	}

	/**
	 * @param IRequest $request
	 * @return IDomElement
	 */
	static function requestDetails( $request ) {
		return (
			section( [ 'class' => 'request-details' ],
				h2( [], [ '🌐', ' ', 'Request' ] ),
				table( [],
					tbody( [],
						tr( [],
							th( [], 'Method' ),
							td( [], $request->getMethod() ),
						),
						tr( [],
							th( [], 'Path' ),
							td( [], $request->getPath() )
						),
						tr( [],
							th( [], 'Requested Path' ),
							td( [], $request->getRequestedPath() )
						),
						tr( [],
							th( [], 'Query' ),
							td( [], $request->getQuery() )
						),
					)
				),
			)
		);
	}

	/**
	 * @param Throwable $throwable
	 * @param IRequest $request
	 * @return IResponse
	 */
	public function serve( $throwable, $request ) {
		return View::errorResponse( $request, $throwable );
	}

}

/**
 * HTML Line item contents for an item in an Excepion stack trace.
 *
 * @param array $trace
 * @return IDomElement
 */
function traceLine( $trace ) {
	return fragment(
		span( [ 'class' => 'trace-source-location' ], traceLocation( $trace ) ),
		' ',
		span( [ 'class' => 'trace-source-symbol ' ], traceSymbol( $trace ) )
	);
}

/**
 * Symbol name from trace array.
 *
 * A symbol name is something like `Fractux\App\Route::route()`.
 * @param array $trace
 * @return IDomElement
 */
function traceSymbol( $trace ) {
	return fragment(
		traceValue( $trace, 'class' ),
		traceValue( $trace, 'type' ),
		traceValue( $trace, 'function',
			/**
			 * @param string $functionName
			 * @return IDomElement
			 */
			function( $functionName ) {
				return span( [ 'class' => 'trace-function' ], $functionName );
			}
		)
	);
}

/**
 * Trace location from trace array.
 *
 * @param array $trace
 * @return IDomElement
 */
function traceLocation( $trace ) {
	return fragment(
		traceValue( $trace, 'file' ),
		traceValue( $trace, 'line',
			/**
			 * @param string $line
			 */
			function( $line ) {
				return ':' . $line;
			}
		)
	);
}

/**
 * Pull a value out of a trace array and optionally customize it with a closure.
 *
 * @param array $trace
 * @param string $key
 * @param closure(string):(string|IDomElement) $format
 * @return string|IDomElement
 */
function traceValue( $trace, $key, $format = null ) {
	if ( key_exists( $key, $trace ) ) {
		return $format === null ? (string) $trace[ $key ] : $format( (string) $trace[ $key ] );
	}
	return '';
}

/**
 * Turn the contents of a file on the filesystem into a data URI.
 *
 * @param string $contents
 * @param string $mime_type
 * @return string
 */
function file_data_uri( $contents, $mime_type = 'text/plain' ) {
	return "data:$mime_type;base64," . base64_encode( $contents );
}
