<?php
namespace Fractux\App;

/**
 * Turns a IRequest into a IResponse
 */
interface IEndpoint {
	/**
	 * @param IRequest $request
	 * @return IResponse $response;
	 */
	function serve( $request );
}
