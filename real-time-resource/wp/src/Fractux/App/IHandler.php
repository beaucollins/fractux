<?php
namespace Fractux\App;

/**
 * Used in the routing system to handle matched (IMatcher) requests.
 *
 * Given a resource (T) and incoming value I (e.g. IRequest), returns
 * the transformed outgoing value (O).
 *
 * @template T
 * @template I
 * @template O
 */
interface IHandler {
	/**
	 * @param T $resource
	 * @param I $request
	 * @return O
	 */
	function serve( $resource, $request );
}

