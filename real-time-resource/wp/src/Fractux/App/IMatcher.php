<?php
namespace Fractux\App;

/**
 * Matches incoming requests in the routing system.
 *
 * Determines if an incoming (I) object should be acted about. If so, the returned value (T)
 * will be used with `IHandler` instances.
 *
 * To indicate that at match has failed, an IMatcher must return an implementation of
 * `IUnmatched`. For convenience `Route::unmatched()` can be used.
 *
 * @template I
 * @template T
 */
interface IMatcher {
	/**
	 * Perform matching operation.
	 *
	 * Returning `IUnmatched` means the match failed.
	 *
	 * @param I $incoming
	 * @return IUnmatched|T
	 */
	function matches( $incoming );
}

