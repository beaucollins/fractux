<?php
namespace Fractux\App;
/**
 * Content that can be output as a string in a response.
 */
interface IRenderable {
	/**
	 * @return string
	 */
	public function __toString();
}
