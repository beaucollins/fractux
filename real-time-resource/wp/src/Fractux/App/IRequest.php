<?php
namespace Fractux\App;
/**
 * Incoming HTTP requests.
 */
interface IRequest {
	/**
	 * @return string
	 */
	function getPath();

	/**
	 * @return string
	 */
	function getRequestedPath();

	/**
	 * @return string
	 */
	function getMethod();

	/**
	 * @return string
	 */
	function getQuery();

	/**
	 * @return null|mixed
	 */
	function getBody();

	/**
	 * @return array<string, string|array<string>>
	 */
	function getHeaders();
}
