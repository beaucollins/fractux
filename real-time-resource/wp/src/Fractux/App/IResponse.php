<?php
namespace Fractux\App;
/**
 * Outgoing HTTP response.
 */
interface IResponse {

	/**
	 * @return int
	 */
	function getStatus();

	/**
	 * @return array<string, (string|array<string>)>
	 */
	function getHeaders();

	/**
	 * @return string
	 */
	function getBody();
}
