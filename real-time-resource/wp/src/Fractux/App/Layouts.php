<?php
namespace Fractux\App;

use Fractux\Html\{Html, IDomElement};
use function Fractux\Html\{div, body, element, html, document, head, title, fragment};
/**
 * Collections of HTML builders.
 */
final class Layouts {

	/**
	 * @param string $name
	 * @return IDomElement
	 */
	public static function hello( $name ) {
		return self::page( fragment(
			div( [], 'Hello, ', $name, '.' ),
			self::formFragment()
		) );
	}

	/**
	 * @return IDomElement
	 */
	public static function debug() {
		return self::page( 'Debug-a-roni and cheese' );
	}

	/**
	 * @return closure(array<string,string>, IRequest):IDomElement
	 */
	public static function greet() {
		return
			/**
			 * @param array<string,string> $path_params
			 * @param IRequest $request
			 * @return IDomElement
			 * @psalm-suppress UnusedClosureParam
			 */
			function( $path_params, $request ) {
				return Layouts::hello( urldecode( $path_params['name'] ) );
			};
	}

	/**
	 * @return IDomElement
	 */
	public static function form() {
		return self::page( self::formFragment() );
	}

	/**
	 * @param IDomElement|string $body
	 * @return IDomElement
	 */
	public static function page( $body ) {
		return (
			document(
				html( [],
					head( [], title( [], 'Give me your name' ) ),
					body( [], $body )
				)
			)
		);
	}

	/**
	 * @return IDomElement
	 */
	public static function formFragment() {
		return element( 'form',
			[ 'method' => 'post', 'action' => '/hello' ],
			element(
				'input', [
					'type' => 'text',
					'name' => 'name',
					'placeholder' => '<hacked>"',
				], []
			),
			element( 'button', [ 'type' => 'submit' ], [ '🚀' ] ),
		);
	}
}
