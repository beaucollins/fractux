<?php
namespace Fractux\App;

/**
 * @template I
 * @template A
 * @template B
 *
 * @implements IMatcher<I,B>
 */
final class MatcherMap implements IMatcher {

	/**
	 * @template Incoming
	 * @template Original
	 * @template Mapped
	 *
	 * @param IMatcher<Incoming,Original> $matcher
	 * @param closure(Original):(Mapped|IUnmatched) $mapper
	 * @return IMatcher<Incoming,Mapped>
	 */
	public static function map( $matcher, $mapper ) {
		/**
		 * @var MatcherMap<Incoming,Original,Mapped>
		 */
		$mapped = new MatcherMap( $matcher, $mapper );
		return $mapped;
	}

	/**
	 * @var IMatcher<I,A>
	 */
	private $matcher;

	/**
	 * @var closure(A):(B|IUnmatched)
	 */
	private $mapper;

	/**
	 * @param IMatcher<I,A> $matcher
	 * @param closure(A):(B|IUnmatched) $mapper
	 */
	function __construct( $matcher, $mapper ) {
		$this->matcher = $matcher;
		$this->mapper = $mapper;
	}

	/**
	 * @param I $incoming
	 * @return B|IUnmatched
	 */
	public function matches( $incoming ) {
		$internal = $this->matcher->matches( $incoming );

		if ( $internal instanceof IUnmatched ) {
			return $internal;
		}

		$mapper = $this->mapper;

		return $mapper( $internal );
	}
}
