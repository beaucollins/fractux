<?php
namespace Fractux\App;

/**
 * Matcher that compares the IRequest::getMethod() value.
 *
 * @template T of 'GET'|'POST'
 * @implements IMatcher<string, T>
 */
final class Method implements IMatcher {

	/**
	 * @var T
	 */
	private $method;

	/**
	 * @param T $method
	 */
	function __construct( $method ) {
		$this->method = $method;
	}

	/**
	 * @param string $incomingMethod
	 * @return T|IUnmatched
	 */
	function matches( $incomingMethod ) {
		if ( $incomingMethod === $this->method ) {
			return $this->method;
		}
		return Route::unmatched();
	}

}
