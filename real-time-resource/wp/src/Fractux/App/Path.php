<?php
namespace Fractux\App;

/**
 * @template T
 * @implements IMatcher<string,T>
 */
final class Path implements IMatcher {

	/**
	 * Matches when $path is literally equal to the IRequest::getPath()
	 *
	 * @param string $path
	 * @return IMatcher<string,string>
	 */
	public static function is( $path ) {
		/**
		 * @var Path<string>
		 */
		return new self(
			/**
			 * @param string $requestedPath
			 * @return string|IUnmatched
			 */
			function( $requestedPath ) use ( $path ) {
				if ( $requestedPath === $path ) {
					return $path;
				}
				return Route::unmatched();
			}
		);
	}

	/**
	 * Compares the path template with a request path.
	 *
	 * If the path template matches, an array af the provided values is returned.
	 *
	 * @param string $pathTemplate
	 * @return Path<array<array-key,string>>
	 */
	public static function like( $pathTemplate ) {
		/**
		 * @var Path<array<array-key,string>>
		 */
		return new self(
			/**
			 * @param string $requestedPath
			 * @return array<array-key,string>|IUnmatched
			 */
			function ( $requestedPath ) use ( $pathTemplate ) {
				return Path::compile( $pathTemplate, $requestedPath );
			}
		);
	}

	/**
	 * Uses the provided closure to perform the matching.
	 *
	 * @template P
	 * @param closure(string):(P|IUnmatched) $pathMatcher
	 * @return Path<P>
	 */
	public static function match( $pathMatcher ) {
		/**
		 * @var Path<P>
		 */
		$matcher = new self( $pathMatcher );
		return $matcher;
	}

	/**
	 * Compares a path template with a path and returns the matched variables.
	 *
	 * @param string $template
	 * @param string $path
	 * @param array<array-key,string> $matches
	 * @return IUnmatched|array<array-key,string>
	 */
	public static function compile( $template, $path, $matches = [] ) {
		$variableIndicator = strpos( $template, ':', 0 );
		if ( $variableIndicator === false ) {
			// No variables in the template, the template and path must match now
			if ( $template === $path ) {
				return $matches;
			} else {
				return Route::unmatched();
			}
		}
		if ( $variableIndicator === 0 ) {
			$variableTerminator = strpos( $template, '/' );
			$variableNameLength = $variableTerminator === false ? strlen( $template ) : $variableTerminator;
			$variableName = substr( $template, 1, $variableNameLength - 1 );

			$valueTerminator = strpos( $path, '/' );
			$valueLength = $valueTerminator === false ? strlen( $path ) : $valueTerminator;
			$value = substr( $path, 0, $valueLength );

			if ( $value === false ) {
				return Route::unmatched();
			}

			if ( $variableName === false ) {
				return Route::unmatched();
			}

			if ( $variableName === '' ) {
				return Route::unmatched();
			}

			return self::compile(
				substr( $template, $variableNameLength ),
				substr( $path, $valueLength ),
				array_merge( $matches, [ $variableName => $value ] )
			);
		}

		$prefix = substr( $template, 0, $variableIndicator );

		if ( strpos( $path, $prefix ) === 0 ) {
			return self::compile(
				substr( $template, $variableIndicator ),
				substr( $path, $variableIndicator ),
				$matches
			);
		}

		return Route::unmatched();
	}

	/**
	 * @var closure(string):(T|IUnmatched)
	 */
	private $condition;

	/**
	 * @param closure(string):(T|IUnmatched) $condition
	 */
	function __construct( $condition ) {
		$this->condition = $condition;
	}

	/**
	 * @param string $path
	 */
	public function matches( $path ) {
		$condition = $this->condition;
		return $condition( $path );
	}

}
