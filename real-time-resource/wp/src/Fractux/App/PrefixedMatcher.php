<?php
namespace Fractux\App;

/**
 * @template O
 * @implements IMatcher<IRequest,O>
 */
final class PrefixedMatcher implements IMatcher {

	/**
	 * @var IMatcher<IRequest,O>
	 */
	private $matcher;

	/**
	 * @var string
	 */
	private $prefix;

	/**
	 * @param string $prefix
	 * @param IMatcher<IRequest,O> $matcher
	 */
	function __construct( $prefix, $matcher ) {
		$this->prefix = $prefix;
		$this->matcher = $matcher;
	}

	/**
	 * @param IRequest $request
	 * @return IUnmatched|O
	 */
	public function matches( $request ) {
		$prefixed = $this->prefixedRequest( $request );

		if ( $prefixed instanceof IUnmatched ) {
			return $prefixed;
		}

		return $this->matcher->matches( $prefixed );
	}

	/**
	 * @param IRequest $request
	 * @return IUnmatched|PrefixedRequest
	 */
	private function prefixedRequest( $request ) {
		if ( strpos( $request->getPath(), $this->prefix ) !== 0 ) {
			return Route::unmatched();
		}

		$rest = substr( $request->getPath(), strlen( $this->prefix ) );

		if ( $rest === false ) {
			return Route::unmatched();
		}

		if ( $rest === '' ) {
			$rest = '/';
		}

		return new PrefixedRequest( $rest, $request );
	}

}
