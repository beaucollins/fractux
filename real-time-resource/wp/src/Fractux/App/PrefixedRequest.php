<?php
namespace Fractux\App;

final class PrefixedRequest implements IRequest {
	/**
	 * @var IRequest
	 */
	private $request;

	/**
	 * @var string
	 */
	private $path;

	/**
	 * @param string $path
	 * @param IRequest $request
	 */
	function __construct( $path, $request ) {
		$this->request = $request;
		$this->path = $path;
	}

	function getPath() {
		return $this->path;
	}

	function getMethod() {
		return $this->request->getMethod();
	}

	function getRequestedPath() {
		return $this->request->getRequestedPath();
	}

	function getQuery() {
		return $this->request->getQuery();
	}

	function getBody() {
		return $this->request->getBody();
	}

	function getHeaders() {
		return $this->request->getHeaders();
	}

}
