<?php
namespace Fractux\App;

final class Redirect implements IResponse {
	/**
	 * @var 301|302
	 */
	private $status;

	/**
	 * @var string
	 */
	private $location;

	/**
	 * @param string $location
	 * @param 301|302 $status
	 */
	function __construct( $location, $status ) {
		$this->location = $location;
		$this->status = $status;
	}

	function getStatus() {
		return $this->status;
	}

	function getHeaders() {
		return [ 'Location' => $this->location ];
	}

	function getBody() {
		return '';
	}
}
