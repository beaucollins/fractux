<?php
namespace Fractux\App;

/**
 * @template A
 * @implements IMatcher<IRequest,IResponse>
 */
final class RequestHandler implements IMatcher {

	/**
	 * @var IHandler<A, IRequest, IResponse>
	 */
	private $handler;

	/**
	 * @var IMatcher<IRequest, A>
	 */
	private $matcher;

	/**
	 * @param IMatcher<IRequest, A> $matcher
	 * @param IHandler<A,IRequest,IResponse> $handler
	 */
	function __construct( $matcher, $handler ) {
		$this->matcher = $matcher;
		$this->handler = $handler;
	}

	/**
	 * @param IRequest $request
	 * @return IResponse|IUnmatched
	 */
	function matches( $request ) {
		/**
		 * @var A
		 */
		$matched = $this->matcher->matches( $request );
		if ( $matched instanceof IUnmatched ) {
			return $matched;
		}
		return $this->handler->serve( $matched, $request );
	}
}
