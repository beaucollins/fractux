<?php
namespace Fractux\App;

use Throwable;

/**
 * @template M of 'GET'|'POST'
 * @template P
 * @implements IMatcher<IRequest, P>
 */
final class Route implements IMatcher {

	/**
	 * @param IRequest $request
	 * @param Throwable $error
	 * @return IResponse
	 */
	public static function error( $request, $error ) {
		return View::errorResponse( $request, $error );
	}

	/**
	 * @template T
	 * @param IMatcher<string,T> $detectPath
	 * @return IMatcher<IRequest,T>
	 */
	public static function get( $detectPath ) {
		return self::requestPath( 'GET', $detectPath );
	}

	/**
	 * @template T
	 * @param IMatcher<string,T> $detectPath
	 * @return IMatcher<IRequest,T>
	 */
	public static function post( $detectPath ) {
		return self::requestPath( 'POST', $detectPath );
	}

	/**
	 * @template R of 'GET'|'POST'
	 * @template T
	 * @param R $method
	 * @param IMatcher<string,T> $detectPath
	 * @return IMatcher<IRequest,T>
	 */
	public static function requestPath( $method, $detectPath ) {
		/**
		 * @var Route<R,T>
		 */
		return new self( new Method( $method ), $detectPath );
	}

	/**
	 * @return IUnmatched
	 */
	public static function unmatched() {
		return new class implements IUnmatched {};
	}

	/**
	 * @template V
	 * @param IMatcher<IRequest,V> $matcher
	 * @param IHandler<V,IRequest,IResponse> $handler
	 * @return IMatcher<IRequest,IResponse>
	 */
	public static function mount( $matcher, $handler ) {
		$requestHandler = new RequestHandler(
			$matcher,
			$handler
		);

		return $requestHandler;
	}

	/**
	 * @template O
	 * @param string $prefix
	 * @param IMatcher<IRequest,O> $route
	 * @return IMatcher<IRequest,O>
	 */
	public static function prefix( $prefix, $route ) {
		/**
		 * @var PrefixedMatcher<O>
		 */
		return new PrefixedMatcher( $prefix, $route );
	}

	/**
	 * @template T
	 * @param closure(T,IRequest):IResponse $handler
	 * @return IHandler<T,IRequest,IResponse>
	 */
	public static function handle( $handler ) {
		return new ClosureHandler( $handler );
	}

	/**
	 * @template T
	 * @param closure(IRequest):(IUnmatched|T) $matcher
	 * @return IMatcher<IRequest,T>
	 */
	public static function match( $matcher ) {
		/**
		 * @var IMatcher<IRequest,T>
		 */
		return new ClosureMatcher( $matcher );
	}

	/**
	 * @template T
	 * @return IMatcher<IRequest, T>
	 */
	public static function neverMatches() {
		/**
		 * @var IMatcher<IRequest, T>
		 */
		$matcher = new ClosureMatcher(
			/**
			 * @param string $request
			 * @return T|IUnmatched
			 * @psalm-suppress UnusedClosureParam
			 */
			function ( $request ) {
				return Route::unmatched();
			}
		);
		return $matcher;
	}

	/**
	 * @template X
	 * @param string $location
	 * @param 301|302 $status
	 * @return IHandler<X,IRequest,IResponse>
	 */
	public static function redirectTo( $location, $status = 301 ) {
		/**
		 * @var IHandler<X,IRequest,IResponse>
		 */
		return new ClosureHandler(
			/**
			 * @param string $path
			 * @param IRequest $request
			 * @return IResponse
			 */
			function() use ( $location, $status ): IResponse {
				return new Redirect( $location, $status );
			}
		);
	}

	/**
	 * @param IMatcher<IRequest, IResponse> ...$routes
	 * @return IMatcher<IRequest, IResponse>
	 */
	static public function combine( ...$routes ) {
		/**
		 * @var IMatcher<IRequest, IResponse>
		 */
		$matcher = new class( $routes ) implements IMatcher {
			/**
			 * @var Array<IMatcher<IRequest,IResponse>>
			 */
			private $routes;

			/**
			 * @param Array<IMatcher<IRequest,IResponse>> $routes
			 */
			function __construct( $routes ) {
				$this->routes = $routes;
			}

			public function matches( $request ) {
				foreach ( $this->routes as $route ) {
					$result = $route->matches( $request );
					if ( $result instanceof IUnmatched ) {
						continue;
					}
					return $result;
				}
				return Route::unmatched();
			}
		};
		return $matcher;
	}

	/**
	 * @param IRequest $request
	 * @param string $message
	 * @return IResponse
	 */
	static public function notFound( $request, $message = 'not found' ) {
		return self::errorResponse( $request, new RouteNotFound( $request, $message ), 404, '❓' );
	}

	/**
	 * @param IRequest $request
	 * @param string $message
	 * @return IResponse
	 */
	static public function notImplemented( $request, $message = 'not implemented' ) {
		return self::errorResponse( $request, new RouteNotImplemented( $message ), 501 );
	}

	/**
	 * @param IRequest $request
	 * @param string $message
	 * @return IResponse
	 */
	static public function badRequest( $request, $message = 'bad request' ) {
		return self::errorResponse( $request, new \RuntimeException( $message ), 400 );
	}

	/**
	 * @param IRequest $request
	 * @param \Throwable $exception
	 * @param int $status
	 * @param string $glyph
	 * @return IResponse
	 */
	static public function errorResponse( $request, $exception, $status = 500, $glyph = '💥' ) {
		return View::errorResponse( $request, $exception, $status, $glyph );
	}

	/**
	 * @var Method<M>
	 */
	private $method;

	/**
	 * @var IMatcher<string,P>
	 */
	private $path;

	/**
	 * @param Method<M> $method
	 * @param IMatcher<string,P> $path
	 */
	private function __construct( $method, $path ) {
		$this->method = $method;
		$this->path = $path;
	}

	/**
	 * @param IRequest $request
	 * @return P|IUnmatched
	 */
	public function matches( $request ) {
		$method = $this->method->matches( $request->getMethod() );
		if ( $method instanceof IUnmatched ) {
			return $method;
		}
		return $this->path->matches( $request->getPath() );
	}
}
