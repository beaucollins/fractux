<?php

namespace Fractux\App;

use Fractux\Html\IDomElement;

final class RouteNotFound extends \RuntimeException {

	/**
	 * @var IRequest
	 */
	private $request;

	/**
	 * @param IRequest $request
	 * @param string $message
	 */
	function __construct( $request, $message ) {
		parent::__construct( $message );
		$this->request = $request;
	}
}
