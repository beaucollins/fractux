<?php
namespace Fractux\App;

use RuntimeException;

final class RouteNotImplemented extends RuntimeException {

}
