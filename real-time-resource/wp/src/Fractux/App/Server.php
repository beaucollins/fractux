<?php
namespace Fractux\App;

use Throwable;

final class Server {

	/**
	 * @param IEndpoint $app
	 * @param IRequest $request
	 * @return void
	 */
	public final static function serve( $app, $request ) {
		$response = self::getResponse( $app, $request );
		self::sendHeaders( $response->getStatus(), $response->getHeaders() );
		echo $response->getBody();
	}

	/**
	 * @param IEndpoint $app
	 * @param IRequest $request
	 * @return IResponse
	 */
	public final static function getResponse( $app, $request ) {
		try {
			return $app->serve( $request );
		} catch ( Throwable $exception ) {
			return Route::error( $request, $exception );
		}
	}

	/**
	 * @param int $status
	 * @param array<string,(string|array<string>)> $headers
	 * @return void
	 */
	private static function sendHeaders( $status, $headers ) {
		foreach ( $headers as $name => $value ) {
			if ( is_array( $value ) ) {
				foreach ( $value as $single ) {
					header( "${name}: ${single}" );
				}
			} else {
				header( "${name}: ${value}" );
			}
		}
		header( 'HTTP/1.1 ' . $status );
	}

}
