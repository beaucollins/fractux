<?php
namespace Fractux\App\Server;

use \Fractux\App\IRequest;
use JsonSerializable;

final class Request implements IRequest, JsonSerializable {

	/**
	 * @return IRequest
	 */
	final public static function buildRequest() {
		$method = (string) array_get( $_SERVER, [ 'REQUEST_METHOD' ], 'GET' );

		$path = (string) array_get( $_SERVER, [ 'REQUEST_URI', 'PATH_INFO' ], '/' );

		/**
		 * @var false|array<string, string|array<string>>
		 */
		$headers = apache_request_headers();

		$queryIndicator = strpos( $path, '?' );

		if ( $queryIndicator === false ) {
			$queryIndicator = strlen( $path );
		}

		return new self(
			$method,
			$headers !== false ? $headers : [],
			substr( $path, 0, $queryIndicator ),
			substr( $path, $queryIndicator ),
			$_POST
		);
	}

	/**
	 * @var string
	 */
	private $path;

	/**
	 * @var array<string, string|array<string>>
	 */
	private $headers;

	/**
	 * @var string
	 */
	private $method;

	/**
	 * @var string
	 */
	private $query;

	/**
	 * @var mixed
	 */
	private $body;

	/**
	 * @param string $method
	 * @param array<string,string|array<string>> $headers
	 * @param string $path
	 * @param string $query
	 * @param mixed $body
	 */
	function __construct( $method, $headers, $path, $query, $body = null ) {
		$this->path = $path;
		$this->method = $method;
		$this->headers = $headers;
		$this->query = $query;
		$this->body = $body;
	}

	public function getPath() {
		return $this->path;
	}

	public function getRequestedPath() {
		return $this->path;
	}

	public function getMethod() {
		return $this->method;
	}

	public function getQuery() {
		return $this->query;
	}

	public function getBody() {
		return $this->body;
	}

	public function getHeaders() {
		return $this->headers;
	}

	public function jsonSerialize() {
		return [
			'method' => $this->method,
			'path' => $this->path,
			'query' => $this->query,
		];
	}
}

/**
 * @param array $array
 * @param array<string> $keys
 * @param string $default
 * @return mixed
 */
function array_get( $array, $keys, $default ) {
	foreach ( $keys as $key ) {
		if ( array_key_exists( $key, $array ) ) {
			return $array[ $key ];
		}
	}
	return $default;
}
