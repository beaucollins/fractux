<?php
namespace Fractux\App;

/**
 * @template T
 * @template I
 * @template O
 * @implements IHandler<T,I,O>
 */
final class StaticResponseHandler implements IHandler {

	/**
	 * @var O
	 */
	private $response;

	/**
	 * @param O $response
	 */
	function __construct( $response ) {
		$this->response = $response;
	}

	/**
	 * @param T $param
	 * @param I $request
	 * @return O
	 */
	public function serve( $param, $request ) {
		return $this->response;
	}
}
