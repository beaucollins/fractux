<?php
namespace Fractux\App;
use Fractux\Html\IDomElement;

/**
 * Utilities for contsructing general purpose IResponse and IHandler content.
 *
 * Alwas output a static string of content:
 *
 * ```php
 *   $view = View::static("Hello world");
 * ```
 *
 * This returns an `IHandler<T>` that can be combined with any `IMatcher<T>`
 * to output a response.
 *
 * ```php
 *   $matcher = Route::get('/hello');
 *
 *   new App( new RequestHandler( $matcher, $view ) );
 * ```
 *
 * @template U
 * @implements IHandler<U,IRequest,IResponse>
 */
abstract class View implements IHandler {
	/**
	 * @template T
	 * @param callable(T, IRequest):IDomElement $responder
	 * @return IHandler<T,IRequest,IResponse>
	 */
	static function respond( $responder ) {
		/**
		 * @var View<T>
		 */
		$handler = new class ( $responder ) extends View {
			/**
			 * @var callable(T, IRequest):IDomElement
			 */
			private $responder;

			/**
			 * @param callable(T, IRequest):IDomElement $responder
			 */
			function __construct( $responder ) {
				$this->responder = $responder;
			}

			function serve( $t, $request ) {
				$responder = $this->responder;
				$content = $responder( $t, $request );
				return View::htmlResponse( $content );
			}
		};
		return $handler;
	}

	/**
	 * @param string|IRenderable $content
	 * @param int $status
	 * @return IResponse
	 */
	public static function htmlResponse( $content, $status = 200 ) {
		return self::staticResponse( $content, $status );
	}

	/**
	 * @param IRequest $request
	 * @param \Throwable $error
	 * @param int $status
	 * @param string $glyph
	 * @return IResponse
	 */
	public static function errorResponse( $request, $error, $status = 500, $glyph = '💥' ) {
		switch ( Accept::from( $request ) ) {
			case Accept::JSON:
				return self::jsonResponse( ErrorResponseHandler::json( $error ), $status );

			case Accept::HTML:
			default:
				return self::htmlResponse( ErrorResponseHandler::document( $request, $error, $glyph ), $status );
		}
	}

	/**
	 * @param array|\stdClass|\JsonSerializable $json
	 * @param int $status
	 * @param array<string,string|string[]> $headers
	 * @return IResponse
	 */
	public static function jsonResponse( $json, $status = 200, $headers = [] ) {
		return self::staticResponse( json_encode( $json ), $status, array_merge( $headers, [ 'content-type' => 'application/json' ] ) );
	}

	/**
	 * @template X
	 * @param string|IRenderable $content
	 * @param int $status
	 * @param array<string,string> $headers
	 * @return IHandler<X,IRequest,IResponse>
	 */
	public static function static( $content, $status = 200, $headers = [] ) {
		/**
		 * @var StaticResponseHandler<X,IRequest,IResponse>
		 */
		return new StaticResponseHandler( View::staticResponse( $content, $status, $headers ) );
	}

	/**
	 * @param string|IRenderable $content
	 * @param int $status
	 * @param array<string,string|string[]> $headers
	 * @return IResponse
	 */
	static public function staticResponse( $content, $status = 200, $headers = [] ) {
		return new class( $content, $status, $headers ) implements IResponse {
			/**
			 * @var IRenderable|string
			 */
			private $content;

			/**
			 * @var int
			 */
			private $status;

			/**
			 * @var array<string, string|string[]>
			 */
			private $headers;

			/**
			 * @param IRenderable|string $content
			 * @param int $status
			 * @param array<string, string|string[]> $headers
			 */
			function __construct( $content, $status, $headers ) {
				$this->content = $content;
				$this->status = $status;
				$this->headers = $headers;
			}

			function getStatus() {
				return $this->status;
			}

			function getHeaders() {
				return $this->headers;
			}

			function getBody() {
				return (string) $this->content;
			}
		};
	}
}
