<?php
namespace Fractux;

use Symfony\Component\Dotenv\Dotenv;

final class Config {

	/**
	 * @return string
	 */
	public static function devServiceURI() {
		return self::getEnv( 'DEV_SERVICE_URI', 'ws://localhost:16385/resource' );
	}

	/**
	 * @param string $var
	 * @param string $default
	 * @return string
	 */
	private static function getEnv( $var, $default ) {
		$val = getenv( $var );
		if ( $val === false ) {
			return $default;
		}
		return $val;
	}

}
