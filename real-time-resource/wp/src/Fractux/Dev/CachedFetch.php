<?php
namespace Fractux\Dev;

use Throwable;
use UnexpectedValueException;

/**
 * @template T
 * @implements IFetch<T>
 */
final class CachedFetch implements IFetch {

	/**
	 * @var IFetch<T>
	 */
	private $fetcher;

	/**
	 * @var bool
	 */
	private $isFetched = false;

	/**
	 * @var null|T|Throwable
	 */
	private $result = null;

	/**
	 * @param IFetch<T> $fetcher
	 */
	function __construct( $fetcher ) {
		$this->fetcher = $fetcher;
	}

	/**
	 * @return T|Throwable
	 */
	public function fetch() {
		if ( $this->isFetched ) {
			if ( $this->result === null ) {
				return new UnexpectedValueException( 'Fetched value is null' );
			}
			return $this->result;
		}

		$result = $this->fetcher->fetch();

		if ( $result === null ) {
			return new UnexpectedValueException( 'Fetched value is null' );
		}

		if ( $result instanceof Throwable ) {
			return $result;
		}

		$this->isFetched = true;
		$this->result = $result;

		return $result;
	}

}
