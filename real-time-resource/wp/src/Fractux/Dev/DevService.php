<?php
namespace Fractux\Dev;

use Fractux\{
	IService,
	IResourceDescriptor,
	IServiceRegistrationStatus,
	IRegistration,
	ServiceChallenge,
	IEncodable,
	IEncryptedData
};
use InvalidArgumentException;
use RuntimeException;
use Throwable;
use GuzzleHttp\{Client, RequestOptions, Exception\RequestException };

final class DevService implements IService {

	const DESCRIPTOR_PATH = '/var/app/data/service-descriptor.json';

	/**
	 * @param mixed $url
	 * @return DevService|Throwable
	 */
	public static function createService( $url ) {
		if ( ! is_string( $url ) ) {
			return new InvalidArgumentException( 'Invalid url' );
		}
		return new self( $url );
	}

	public function getGUID() {
		/**
		 * @var string
		 */
		return getenv( 'DEV_SERVICE_GUID', true );
	}

	public function getRealtimeURI() {
		return \Fractux\Config::devServiceURI();
	}

	/**
	 * @var string $url
	 */
	private $url;

	private function __construct( string $url ) {
		$this->url = $url;
	}

	/**
	 * @inheritDoc
	 */
	public function getRegistrationStatus() {
		if ( ! file_exists( self::DESCRIPTOR_PATH ) ) {
			return 'unregistered';
		}
		return 'connected';
	}

	/**
	 * @inheritDoc
	 */
	public function getPublicKey() {
		$client = new Client();
		$response = $client->request( 'GET', $this->getGUID() );
		/**
		 * @var array<string, mixed>
		 */
		$descriptor = json_decode( $response->getBody()->getContents(), true );
		/**
		 * @var string
		 */
		$publicKey = $descriptor['publicKey'];

		return sodium_hex2bin( $publicKey );
	}

	/**
	 * @inheritDoc
	 */
	public function registerResource( $keystore, $descriptor ) {
		$request = $descriptor->register( $this, $keystore );
		$client = new Client();
		$response = $client->request( 'POST', $this->getRegistrationURL(), [ RequestOptions::BODY => $request->encode() ] );
		$contents = $response->getBody()->getContents();

		file_put_contents( self::DESCRIPTOR_PATH, $contents );

		return new class( $contents ) implements IRegistration {
			/**
			 * @var string
			 */
			private $contents;

			/**
			 * @param string $contents
			 */
			function __construct( $contents ) {
				$this->contents = $contents;
			}

			function encode() {
				return $this->contents;
			}
		};
	}

	/**
	 * @inheritDoc
	 */
	public function createChallengeResponse( $keystore, $descriptor, $encodedChallenge ) {
		return $descriptor->createChallengeResponse( $this, $keystore, $encodedChallenge );
	}

	/**
	 * @inheritDoc
	 */
	public function sendMessage( $keystore, $descriptor, $message ) {
		switch ( $this->getRegistrationStatus() ) {
			case 'connected':
				/**
				 * @var array<string, mixed>
				 */
				$configuration = json_decode( file_get_contents( self::DESCRIPTOR_PATH ), true );

				/**
				 * @var string
				 */
				$servicePublicKey = $configuration['publicKey'];
				error_log( 'Configuration: ' . json_encode( $configuration ) );
				// 1) create an encrypted message
				$message = $keystore->encryptContent( $message, sodium_hex2bin( $servicePublicKey ) );
				if ( $message instanceof \Throwable ) {
					return $message;
				}

				/**
				 * @var string
				 */
				$deliveryService = $configuration['deliveryService'];

				try {
					$body = new class( $descriptor->getUrl(), $message ) implements IEncodable {
						/**
						 * @var string
						 */
						private $url;
						/**
						 * @var IEncryptedData
						 */
						private $message;

						/**
						 * @param string $url
						 * @param IEncryptedData $message
						 */
						function __construct( $url, $message ) {
							$this->url = $url;
							$this->message = $message;
						}

						function encode() {
							return json_encode( [
								'resource' => $this->url,
								'nonce' => $this->message->getNonce(),
								'envelope' => $this->message->getEnvelope(),
							] );
						}
					};
					$response = ( new Client() )->request( 'POST', $deliveryService, [ RequestOptions::BODY => $body->encode() ] );
					$content = $response->getBody()->getContents();
					return new class( $content ) implements IEncodable {
						/**
						 * @var string
						 */
						private $content;

						/**
						 * @param string $content
						 */
						function __construct( $content ) {
							$this->content = $content;
						}

						function encode() {
							return $this->content;
						}
					};
				} catch ( RequestException $error ) {
					unlink( self::DESCRIPTOR_PATH );
					return $error;
				}

				// 2) send it to where it needs to go
				// $descriptor->prepareMessage( $this, $keystore, $message );
				return new \RuntimeException( 'Not implemented' );
			default:
				return new \RuntimeException( 'Service not registered' );
		}
	}

	/**
	 * @return string
	 */
	private function getRegistrationURL() {
		$client = new Client();
		$response = $client->request( 'GET', $this->getGUID() );
		/**
		 * @var array<string, mixed>
		 */
		$descriptor = json_decode( $response->getBody()->getContents(), true );
		/**
		 * @var string
		 */
		$url = $descriptor['registrationUrl'];

		return $url;
	}
}
