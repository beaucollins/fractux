<?php
namespace Fractux\Dev;

use UnexpectedValueException;

/**
 * @implements IFetch<string>
 */
final class FetchFileContents implements IFetch {
	/**
	 * @param string $path
	 * @return IFetch<string>
	 */
	public static function fromPath( $path ) {
		/**
		 * @var IFetch<string>
		 */
		return new self( $path );
	}

	/**
	 * @var string
	 */
	private $path;

	/**
	 * @param string $path
	 */
	private function __construct( $path ) {
		$this->path = $path;
	}

	public function fetch() {
		$contents = file_get_contents( $this->path );
		if ( $contents === false ) {
			return new UnexpectedValueException( 'Failed to read contents from: ' . $this->path );
		}
		return $contents;
	}

}
