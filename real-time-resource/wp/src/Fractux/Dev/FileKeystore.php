<?php
namespace Fractux\Dev;

use Fractux\IKeystore;
use InvalidArgumentException;
use stdClass;
use Throwable;

/**
 * Implementation of IKeystore that reads/writes to a file on the local
 * filesystem.
 *
 * Not intended for production usage.
 */
final class FileKeystore extends SodiumKeystore {
	/**
	 * @param mixed $path
	 * @return IKeystore|Throwable
	 */
	public static function createKeystore( $path ) {
		if ( ! is_string( $path ) ) {
			return new InvalidArgumentException( 'path is not valid' );
		}
		return new self( $path );
	}

	/**
	 * @var string
	 */
	private $path;

	/**
	 * @var CachedFetch<string>
	 */
	private $reader;

	/**
	 * @param string $secret
	 * @param string $path
	 */
	private function __construct( $path ) {
		$this->path = $path;
		$this->reader = self::createReader( $path );

		$this->init();
	}

	/**
	 * @return string
	 */
	public function getKeyPair() {
		$data = $this->read();

		if ( $data instanceof Throwable ) {
			throw $data;
		}

		return $data;
	}

	/**
	 * @return void
	 */
	private function generateKeypair() {
		$keypair = sodium_crypto_sign_keypair();
		file_put_contents( $this->path, $keypair );

		// Clear the cached fetch
		$this->resetReader();

		// Reading also validates the contents?
		$this->read();
	}

	/**
	 * Resets the reader for an uncached fetch.
	 *
	 * @return void
	 */
	private function resetReader() {
		$this->reader = self::createReader( $this->path );
	}

	/**
	 * @return string|Throwable
	 */
	private function read() {
		return $this->reader->fetch();
	}

	/**
	 * @return void
	 */
	private function init() {
		if ( ! file_exists( $this->path ) ) {
			$dir = dirname( $this->path );
			if ( ! file_exists( $dir ) ) {
				if ( mkdir( $dir, 0777, true ) === false ) {
					throw new \RuntimeException( 'Unable to create keystore directory: ' . $dir );
				};
			}
			$this->generateKeypair();
		}
	}

	/**
	 * @param string $path
	 * @return CachedFetch<string>
	 */
	private static function createReader( $path ) {
		/**
		 * @var CachedFetch<string>
		 */
		return new CachedFetch(
			FetchFileContents::fromPath( $path )
		);
	}
}
