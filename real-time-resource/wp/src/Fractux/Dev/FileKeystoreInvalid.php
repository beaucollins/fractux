<?php
namespace Fractux\Dev;

use UnexpectedValueException;

final class FileKeystoreInvalid extends UnexpectedValueException {
}
