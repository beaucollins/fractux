<?php
namespace Fractux\Dev;

use Throwable;

/**
 * @template T
 */
interface IFetch {
	/**
	 * @return T|Throwable
	 */
	function fetch();
}
