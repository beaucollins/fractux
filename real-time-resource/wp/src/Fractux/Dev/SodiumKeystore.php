<?php
namespace Fractux\Dev;

use Fractux\{IKeystore, IRegistrationRequest, IEncryptedData};
use RuntimeException;

/**
 * Libsodium implementation that uses the Key Exchange (sodium_crypto_kx_* apis)
 * to provide encrypted communication between the Real-Time service this resource.
 */
abstract class SodiumKeystore implements IKeystore {

	/**
	 * @return string
	 */
	public abstract function getKeyPair();

	/**
	 * @return string
	 */
	public function getClientPublicKey() {
		return sodium_crypto_kx_publickey( $this->getKXKeypair() );
	}

	/**
	 * @return string
	 */
	public function getKXKeypair() {
		$baseKey = $this->getKeyPair();
		$secret = sodium_crypto_sign_secretkey( $baseKey );
		return sodium_crypto_kx_seed_keypair( substr( $secret, 0, SODIUM_CRYPTO_KX_SEEDBYTES ) );
	}

	public function getPublicKey() {
		return sodium_crypto_sign_publickey( $this->getKeyPair() );
	}

	public function signContent( $content ) {
		return sodium_crypto_sign_detached(
			$content,
			sodium_crypto_sign_secretkey( $this->getKeyPair() )
		);
	}

	public function verifyContent( $content, $signature ) {
		return sodium_crypto_sign_verify_detached(
			$signature,
			$content,
			$this->getPublicKey()
		);
	}

	/**
	 * This ties the registration to a single service. What would this mean for
	 * federated services?
	 *
	 * Assuming:
	 *
	 * A: Resource
	 * B: Service
	 * C: Service federated with B
	 *
	 * If C needs to communicate with A, the data needs to be sent using A<->B's crypto
	 * because the keypair trust is between A<->B.
	 *
	 * Either we need to relax registration requirements or allow a federated service
	 * to say "hey B trusts me so you should too". That might mean C will need to ask B
	 * to encrypt/decrypt the payload on its behalf and the send it to A.
	 *
	 * While technically feasible is this:
	 * 1) Secure?
	 * 2) Performant?
	 * 3) Overly complex?
	 *
	 * The ultimate use of this is to allow a user of A's application to subscribe to a
	 * realtime stream that A publishes via B.
	 *
	 * A.x = Browser client of A's application.
	 * - A gives A.x a subscription token
	 * - A.x connects to B with this token. This token should identify which resource it belongs to
	 *    - subscription.resource = URL?
	 *    - subscription.token = A<->B encrypted value?
	 * - B is already registred with A so it knows how to decrypt the token. It immediately starts
	 *    publishing real-time resource content to A.x
	 * - B is not registered with A, does it try to or simply inform the client things aren't kosher
	 */
	public function signRegistration( $servicePublicKey, $content ) {
		$session = $this->createServiceSession( $servicePublicKey );
		// [rx, tx]
		// $clientKeypair = sodium_crypto_kx_client_session_keys( $keypair, $servicePublicKey );
		// $clientPublicKey = sodium_crypto_kx_publickey( $keypair );

		// $nonce = random_bytes( SODIUM_CRYPTO_SECRETBOX_NONCEBYTES );
		// $payload = sodium_crypto_secretbox( $content, $nonce, $clientKeypair[1] );

		$encrypted = $session->encrypt( $content );

		return new class( $session->getPublicKey(), $encrypted ) implements IRegistrationRequest {

			/**
			 * @var string
			 */
			private $publicKey;

			/**
			 * @var IEncryptedData
			 */
			private $encrypted;

			/**
			 * @param string $publicKey
			 * @param IEncryptedData $encrypted
			 */
			function __construct( $publicKey, $encrypted ) {
				$this->publicKey = $publicKey;
				$this->encrypted = $encrypted;
			}

			public function encode() {
				return json_encode(
					[
						'publicKey' => sodium_bin2hex( $this->publicKey ),
						'envelope' => $this->encrypted->getEnvelope(),
						'nonce' => $this->encrypted->getNonce(),
					],
					JSON_FORCE_OBJECT
				);
			}
		};
	}

	/**
	 * @inheritDoc
	 */
	public function decryptContent( $encrypted, $publicKey ) {
		return $this->createServiceSession( $publicKey )->decrypt( $encrypted );
	}

	/**
	 * @inheritDoc
	 */
	public function encryptContent( $message, $publicKey ) {
		return $this->createServiceSession( $publicKey )->encrypt( $message->encode() );
	}

	/**
	 * @inheritDoc
	 */
	public function createServiceSession( $servicePublicKey ) {
		return SodiumSession::createClientSession( $servicePublicKey, $this->getKXKeypair() );
	}
}
