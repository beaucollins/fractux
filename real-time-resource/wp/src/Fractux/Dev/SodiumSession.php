<?php
namespace Fractux\Dev;

use Fractux\{IServiceSession, IEncryptedData};
use stdClass;
use Throwable;
use RuntimeException;

final class SodiumSession implements IServiceSession {

	/**
	 * @param string $servicePublickKey
	 * @param string $kxKeypair
	 * @return IServiceSession
	 */
	public static function createClientSession( $servicePublickKey, $kxKeypair ) {
		$keypair = sodium_crypto_kx_client_session_keys( $kxKeypair, $servicePublickKey );
		return new self( $keypair, sodium_crypto_kx_publickey( $kxKeypair ) );
	}

	/**
	 * @param string $clientPublicKey
	 * @param string $kxKeypair
	 * @return IServiceSession
	 */
	public static function createServerSession( $clientPublicKey, $kxKeypair ) {
		$keypair = sodium_crypto_kx_server_session_keys( $kxKeypair, $clientPublicKey );
		return new self( $keypair, sodium_crypto_kx_publickey( $kxKeypair ) );
	}

	/**
	 * @param mixed $obj
	 * @param string $property
	 * @return string|Throwable
	 */
	public static function fetchProperty( $obj, $property ) {

		if ( ! $obj instanceof stdClass ) {
			return new RuntimeException( 'Expeted object' );
		}

		if ( ! property_exists( $obj, $property ) ) {
			return new RuntimeException( 'Expected property "' . $property . '" not present.' );
		}

		/**
		 * @var mixed
		 */
		$val = $obj->$property;

		if ( ! is_string( $val ) ) {
			return new RuntimeException( 'Property "' . $property . 'not expected type.' );
		}

		return $val;
	}

	/**
	 * @param string $encoded
	 * @return IEncryptedData|Throwable
	 */
	public static function decode( $encoded ) {
		/**
		 * @var mixed
		 */
		$decoded = json_decode( $encoded, false );

		if ( ! $decoded instanceof stdClass ) {
			return new RuntimeException( 'Unable to decode.' );
		}

		$nonce = self::fetchProperty( $decoded, 'nonce' );

		if ( $nonce instanceof Throwable ) {
			return $nonce;
		}

		$envelope = self::fetchProperty( $decoded, 'envelope' );
		if ( $envelope instanceof Throwable ) {
			return $envelope;
		}

		return new class( $nonce, $envelope ) implements IEncryptedData {
			/**
			 * @var string
			 */
			private $nonce;

			/**
			 * @var string
			 */
			private $envelope;

			/**
			 * @param string $nonce
			 * @param string $envelope
			 */
			function __construct( $nonce, $envelope ) {
				$this->nonce = $nonce;
				$this->envelope = $envelope;
			}

			function getNonce() {
				return $this->nonce;
			}

			function getEnvelope() {
				return $this->envelope;
			}
		};
	}

	/**
	 * @var array<string>
	 */
	private $keypair;

	/**
	 * @var string
	 */
	private $publicKey;

	/**
	 * @param array<string> $keypair
	 * @param string $publicKey
	 */
	private function __construct( $keypair, $publicKey ) {
		$this->keypair = $keypair;
		$this->publicKey = $publicKey;
	}

	public function getPublicKey() {
		return $this->publicKey;
	}

	/**
	 * @return array<string>
	 */
	public function getKeypair() {
		return $this->keypair;
	}

	/**
	 * @return string
	 */
	private function getReceiveKey() {
		return $this->getKeypair()[0];
	}

	/**
	 * @return string
	 */

	private function getTransmitKey() {
		return $this->getKeypair()[1];
	}

	public function encrypt( $data ) {
		$nonce = random_bytes( SODIUM_CRYPTO_SECRETBOX_NONCEBYTES );
		$envelope = sodium_crypto_secretbox( $data, $nonce, $this->getTransmitKey() );
		return new class( sodium_bin2hex( $nonce ), sodium_bin2hex( $envelope ) ) implements IEncryptedData {

			/**
			 * @var string
			 */
			private $nonce;

			/**
			 * @var string
			 */
			private $envelope;

			/**
			 * @param string $nonce
			 * @param string $envelope
			 */
			function __construct( $nonce, $envelope ) {
				$this->nonce = $nonce;
				$this->envelope = $envelope;
			}

			function getNonce() {
				return $this->nonce;
			}

			function getEnvelope() {
				return $this->envelope;
			}
		};
	}

	public function decrypt( $encrypted ) {
		$content = sodium_crypto_secretbox_open(
			sodium_hex2bin( $encrypted->getEnvelope() ),
			sodium_hex2bin( $encrypted->getNonce() ),
			$this->getReceiveKey()
		);
		if ( $content === false ) {
			return new RuntimeException( 'Unable to decrypt content: ' . print_r( $encrypted ) );
		}
		return $content;
	}
}
