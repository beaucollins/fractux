<?php
namespace Fractux\Dev;

use Fractux\{IResourceDescriptor, ServiceChallenge, IEncodable, IEncryptedData};
use Throwable;
use InvalidArgumentException;
use RuntimeException;

final class WebhookResource implements IResourceDescriptor {

	/**
	 * @param mixed $url
	 * @return Throwable|IResourceDescriptor
	 */
	public static function create( $url ) {
		if ( ! is_string( $url ) ) {
			return new InvalidArgumentException( 'Invalid Webhook url' );
		}
		return new self( $url );
	}

	/**
	 * @var string
	 */
	private $url;

	/**
	 * @param string $url
	 */
	private function __construct( $url ) {
		$this->url = $url;
	}

	public function getURL() {
		return $this->url;
	}

	/**
	 * Based on the given service configuration and the keystore crypto services
	 * perform the necessary steps to complete registration for a WebhookResource.
	 */
	public function register( $service, $keystore ) {
		// Payload for registration
		$config = json_encode(
			[
				'url' => $this->url,
				'api' => 'webhook',
				'v' => '1.0.0',
			],
			JSON_FORCE_OBJECT
		);

		return $keystore->signRegistration( $service->getPublicKey(), $config );
	}

	/**
	 * Decrypt, encrypt, respond
	 */
	public function createChallengeResponse( $service, $keystore, $encodedChallenge ) {
		$challenge = ServiceChallenge::decode( $encodedChallenge );
		$session = $keystore->createServiceSession( $service->getPublicKey() );
		if ( $challenge instanceof Throwable ) {
			return $challenge;
		}
		$decrypted = $session->decrypt( $challenge );
		return new class( $session->encrypt( $decrypted ) ) implements IEncodable {

			/**
			 * @var IEncryptedData
			 */
			private $encrypted;

			/**
			 * @param IEncryptedData $encrypted
			 */
			function __construct( $encrypted ) {
				$this->encrypted = $encrypted;
			}

			function encode() {
				return json_encode(
					[
						'nonce' => $this->encrypted->getNonce(),
						'envelope' => $this->encrypted->getEnvelope(),
					],
					JSON_FORCE_OBJECT
				);
			}
		};
	}
}
