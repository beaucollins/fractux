<?php
namespace Fractux\Html;

/**
 * HTML with escaped attributes and content.
 */
final class Element extends SafeContent {

	const ELEMENTS = [
		'a' => [ 'inline' => true ],
		'abbr' => [ 'inline' => true ],
		'acronym' => [ 'inline' => true ],
		'audio' => [ 'inline' => true ],
		'b' => [ 'inline' => true ],
		'bdi' => [ 'inline' => true ],
		'bdo' => [ 'inline' => true ],
		'big' => [ 'inline' => true ],
		'br' => [ 'inline' => true ],
		'button' => [ 'inline' => true ],
		'canvas' => [ 'inline' => true ],
		'cite' => [ 'inline' => true ],
		'code' => [ 'inline' => true ],
		'data' => [ 'inline' => true ],
		'datalist' => [ 'inline' => true ],
		'del' => [ 'inline' => true ],
		'dfn' => [ 'inline' => true ],
		'em' => [ 'inline' => true ],
		'embed' => [ 'inline' => true ],
		'i' => [ 'inline' => true ],
		'iframe' => [ 'inline' => true ],
		'img' => [ 'inline' => true ],
		'input' => [ 'inline' => true ],
		'ins' => [ 'inline' => true ],
		'kbd' => [ 'inline' => true ],
		'label' => [ 'inline' => true ],
		'mark' => [ 'inline' => true ],
		'meter' => [ 'inline' => true ],
		'noscript' => [ 'inline' => true ],
		'object' => [ 'inline' => true ],
		'output' => [ 'inline' => true ],
		'picture' => [ 'inline' => true ],
		'progress' => [ 'inline' => true ],
		'q' => [ 'inline' => true ],
		'ruby' => [ 'inline' => true ],
		's' => [ 'inline' => true ],
		'samp' => [ 'inline' => true ],
		// 'script' => [ 'inline' => true ],
		'select' => [ 'inline' => true ],
		'slot' => [ 'inline' => true ],
		'small' => [ 'inline' => true ],
		'span' => [ 'inline' => true ],
		'strong' => [ 'inline' => true ],
		'sub' => [ 'inline' => true ],
		'sup' => [ 'inline' => true ],
		'svg' => [ 'inline' => true ],
		'template' => [ 'inline' => true ],
		'textarea' => [ 'inline' => true ],
		'time' => [ 'inline' => true ],
		'u' => [ 'inline' => true ],
		'tt' => [ 'inline' => true ],
		'var' => [ 'inline' => true ],
		'video' => [ 'inline' => true ],
		'wbr' => [ 'inline' => true ],
	];

	/**
	 * @var string
	 */
	private $elementName;

	/**
	 * @var array<string, string>
	 */
	private $attributes;

	/**
	 * @var array<array<(string|IDomElement)>|string|IDomElement>
	 */
	private $children;

	/**
	 * @param string $elementName
	 * @param array<string, string> $attributes
	 * @param array<string|IDomElement>|string|IDomElement ...$children
	 */
	function __construct( $elementName, $attributes, ...$children ) {
		$this->elementName = $elementName;
		$this->attributes = $attributes;
		$this->children = $children;
	}

	public function render( $options ) {
		$name = SafeString::escape( $this->elementName );
		$childOptions = $options->descend();
		$space = $options->whitespace ? self::pad( $options ) : '';
		$leading = $space;
		$trailing = count( $this->children ) > 1 ? $leading : '';

		$attributes = self::buildAttributes( $this->attributes );

		$isInlineElement = self::isInlineElementType( $this->elementName );

		$children = $this->renderChildren( $childOptions );
		if ( $isInlineElement ) {
			return Node::of( 'inline', <<<HTML
			<$name$attributes>{$children}</$name>
			HTML );
		}

		if ( ( count( $this->children ) === 0 || $children->type !== 'block' ) ) {
			return Node::of( 'block', <<<HTML
			{$leading}<{$name}{$attributes}>{$children}</{$name}>
			HTML );
		}
		return Node::of( 'block', <<<HTML
		{$leading}<{$name}{$attributes}>
		{$children}{$trailing}
		{$leading}</{$name}>
		HTML );
	}

	/**
	 * @param RenderOptions $options
	 * @return Node
	 */
	private function renderChildren( $options ) {
		$fragment = Fragment::from( $this->children );
		return $fragment->render( $options );
	}

	/**
	 * @param array<string,string> $attributes
	 * @return IDomElement
	 */
	public static function buildAttributes( $attributes ) {
		$result = '';
		foreach ( $attributes as $name => $value ) {
			$result .= $result === '' ? '' : ' ';
			$result .= SafeString::escape( $name ) . '="' . SafeString::escape( $value ) . '"';
		}
		return Html::unsafe( $result === '' ? '' : ' ' . $result );
	}

	/**
	 * @param RenderOptions $options
	 * @param string $character
	 * @return string
	 */
	public static function pad( $options, $character = "\t" ) {
		$pad = '';
		for ( $i = 0; $i < $options->depth; $i++ ) {
			$pad .= $character;
		}
		return $pad;
	}

	/**
	 * @param string $elementName
	 * @return bool
	 */
	public static function isInlineElementType( $elementName ) {
		/**
		 * Default to block level elements?
		 */
		if ( ! key_exists( $elementName, self::ELEMENTS ) ) {
			return false;
		}
		$config = self::ELEMENTS[ $elementName ];

		return $config['inline'];
	}

}
