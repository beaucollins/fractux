<?php
namespace Fractux\Html;

/**
 * Renders a list of HTML content nodes.
 *
 * String elements are escaped via `SafeString::escape()`, otherwise IDomElement nodes
 * are rendered with `IDomElement::render( RenderOptions $options ): string`.
 *
 *    echo new Fragment( [ 'hello', ' <b>World</b> ', element( 'em', [], "it's me." ) ] );
 *
 * Produces
 *
 *    hello &lt;b&gt;World&lt/b&gt> <em>it's me</em>.
 */
final class Fragment extends SafeContent {

	/**
	 * @param string|IDomElement|array<(string|IDomElement)>|array<array<string|IDomElement>> $nodes
	 * @return IDomElement
	 */
	public static function from( $nodes ) {
		if ( $nodes instanceof IDomElement ) {
			return $nodes;
		}

		if ( is_string( $nodes ) ) {
			return SafeString::escape( $nodes );
		}

		if ( count( $nodes ) > 1 ) {
			return new self( $nodes );
		}

		if ( count( $nodes ) === 0 ) {
			return new self( [] );
		}

		// If we have [ [ [ [ $thing ] ] ] ] it will simply be $thing
		$node = $nodes[0];
		if ( $node instanceof IDomElement ) {
			return $node;
		}
		if ( is_string( $node ) ) {
			return SafeString::escape( $node );
		}
		return self::from( $node );

	}

	/**
	 * @var array<array<IDomElement|mixed>|IDomElement|mixed> $children
	 */
	private $children;

	/**
	 * @param array<array<IDomElement|mixed>|IDomElement|mixed> $children
	 */
	private function __construct( $children ) {
		$this->children = $children;
	}

	public function render( $options ) {
		return array_reduce(
			$this->children,
			/**
			 * @param array{content:Node,previous:?Node} $context
			 * @param array<string|IDomElement>|IDomElement|string $child
			 * @return array{content:Node,previous:?Node}
			 */
			function( $context, $child ) use ( $options ) {
				if ( is_array( $child ) ) {
					$renderable = Fragment::from( $child );
				} else {
					$renderable = $child instanceof IDomElement ? $child : SafeString::escape( $child );
				}
				$rendered = $renderable->render( $options );
				$separate = $rendered->type === 'block' && $context['previous'] && $context['previous']->type === 'block' ? "\n" : '';
				return [
					'content' => Node::of(
						$context['content']->type !== 'block' ? $context['content']->type : $rendered->type,
						$context['content'] . $separate . $rendered
					),
					'previous' => $rendered,
				];
			},
			[
				'previous' => null,
				'content' => Node::of( 'block', '' ),
			]
		)['content'];
	}
};
