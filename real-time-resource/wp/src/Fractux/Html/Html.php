<?php
namespace Fractux\Html;
/**
 * Create safe Html output.
 *
 *     use Fractux\App\Html\Html;
 *
 *     $document = document(
 *       html( [], [
 *         head( [], [
 *            title( [], 'Title of page' )
 *          ] )
 *          body( [],
 *            div( [], 'Injecting content <i>boom</i>' )
 *          )
 *       ] )
 *     )
 *
 *  Rendering the document will output "safe" HTML:
 *
 *      <DOCTYPE html>
 *      <html>
 *         <head>
 *           <title>Hello Wordl</title>
 *         </head>
 *         <body>
 *           <div>Injecting content &gt;i&lt;</div>
 *         </body>
 *      </html>
 */
final class Html {
	/**
	 * Build an Element with the given element name, attributes, and children.
	 *
	 *     element( 'span', ['class' => 'my-thing'], 'Hello World' )->__toString();
	 *
	 * Produces:
	 *
	 *     <span class="my-thing">Hello World</span>
	 *
	 * @param string $elementName
	 * @param array<string, string> $attributes
	 * @param string|IDomElement|array<IDomElement|string> ...$child
	 * @return IDomElement
	 */
	public static function element( $elementName, $attributes, ...$child ) {
		return new Element( $elementName, $attributes, ...$child );
	}

	/**
	 * Special element that outputs the HTML5 doctype. Used by `Html::document()`.
	 *
	 * @return IDomElement
	 */
	public static function doctype() {
		return self::unsafe( "<!DOCTYPE html>\n" );
	}

	/**
	 * @param string|IDomElement $html
	 * @return IDomElement
	 */
	public static function document( $html ) {
		return self::fragment(
			[
				Html::doctype(),
				$html,
			]
		);
	}

	/**
	 * @param array<IDomElement|string>|IDomElement|string ...$children
	 * @return IDomElement
	 */
	public static function fragment( ...$children ) {
		return Fragment::from( $children );
	}

	/**
	 * @param mixed $content
	 * @return IDomElement
	 */
	public static function escape( $content ) {
		return SafeString::escape( $content );
	}

	/**
	 * @param mixed $content
	 * @return IDomElement
	 */
	public static function unsafe( $content ) {
		return new LiteralContent( $content );
	}

}
