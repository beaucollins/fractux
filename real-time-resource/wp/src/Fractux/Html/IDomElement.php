<?php
namespace Fractux\Html;

use Fractux\App\IRenderable;

/**
 * Content that is safe to be rendered within an HTML document.
 */
interface IDomElement extends IRenderable {
	/**
	 * Rendered value of the content.
	 *
	 * @param RenderOptions $options
	 * @return Node
	 */
	function render( $options );
}
