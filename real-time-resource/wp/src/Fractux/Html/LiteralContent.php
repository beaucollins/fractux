<?php
namespace Fractux\Html;

/**
 * Outputs the content coerced into string form _as is_.
 *
 * This is how to short-ciruit HTML escaping of IDomElement and should
 * only be used via `\Fractux\Html\safe()` or `\Fractux\Html\Html::safe()`.
 *
 * It is the responsibilty of the the user of `LiteralContent` to perform
 * the required escaping to prevent HTML injection attacks.
 *
 *     echo Html::safe( '<script type="
 * text/javascript">console.log( "game over" );</script> );
 *
 * @internal
 */
class LiteralContent extends SafeContent {
	/**
	 * @var mixed
	 */
	private $content;

	/**
	 * @param mixed $content
	 */
	function __construct( $content ) {
		$this->content = $content;
	}

	public function render( $options ) {
		return Node::of( 'text', (string) $this->content );
	}
};
