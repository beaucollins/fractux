<?php
namespace Fractux\Html;

/**
 * @internal
 */
final class Node {

	/**
	 * @param 'text'|'inline'|'block' $type
	 * @param string $content
	 * @return self
	 */
	public static function of( $type, $content ) {
		return new self( $type, $content );
	}

	/**
	 * @readonly
	 * @var 'text'|'inline'|'block'
	 */
	public $type;

	/**
	 * @var string
	 */
	private $content;

	/**
	 * @param 'text'|'inline'|'block' $type
	 * @param string $content
	 */
	private function __construct( $type, $content ) {
		$this->type = $type;
		$this->content = $content;
	}

	public function __toString() {
		return $this->content;
	}

}
