<?php
namespace Fractux\Html;

/**
 * Options for rendering HTML IDomElement elements.
 *
 * Constrols the whitespace handling to produce "pretty" HTML.
 */
final class RenderOptions {

	/**
	 * @param int $depth
	 * @return RenderOptions
	 */
	public static function options( $depth = 0 ) {
		return new self( 0, true );
	}

	/**
	 * @readonly
	 * @var int
	 */
	public $depth;

	/**
	 * @readonly
	 * @var bool
	 */
	public $whitespace;

	/**
	 * @readonly
	 * @var string
	 */
	public $charset = 'UTF-8';

	/**
	 * @param int $depth
	 * @param bool $whitespace
	 * @param string $charset
	 */
	private function __construct( $depth = 0, $whitespace = false, $charset = 'UTF-8' ) {
		$this->depth = $depth;
		$this->whitespace = $whitespace;
		$this->charset = $charset;
	}

	/**
	 * @return self
	 */
	public function descend() {
		return new self( $this->depth + 1, $this->whitespace );
	}
}
