<?php
namespace Fractux\Html;

/**
 * An IDomElement that implements IRenderable.
 *
 * Inheriting from SafeContent then requires the subclass to implement
 *
 *    public function render( RenderOptions $options ): string;
 */
abstract class SafeContent implements IDomElement {
	function __toString() {
		return (string) $this->render( RenderOptions::options() );
	}
}
