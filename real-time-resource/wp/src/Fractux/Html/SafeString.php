<?php
namespace Fractux\Html;

/**
 * Implementation of SafeContent that escapes html characters.
 */
final class SafeString extends SafeContent {

	/**
	 * @param mixed $content
	 * @return IDomElement
	 */
	static function escape( $content ) {
		return new self( $content );
	}

	/**
	 * @var mixed
	 */
	private $content;

	/**
	 * @param mixed $content
	 */
	private function __construct( $content ) {
		$this->content = $content;
	}

	public function render( $options ) {
		return Node::of( 'text', htmlspecialchars( (string) $this->content ) );
	}
}
