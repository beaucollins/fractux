<?php
namespace Fractux\Html;

/**
 * An Element that has no child nodes.
 */
final class SelfClosingElement extends SafeContent {

	/**
	 * @var string
	 */
	private $elementName;

	/**
	 * @var array<string,string>
	 */
	private $attributes;

	/**
	 * @param string $elementName
	 * @param array<string,string> $attributes
	 */
	function __construct( $elementName, $attributes ) {
		$this->elementName = $elementName;
		$this->attributes = $attributes;
	}

	function render( $options ) {
		$name = SafeString::escape( $this->elementName );
		$space = $options->whitespace ? Element::pad( $options ) : '';
		$leading = $space;
		$attributes = Element::buildAttributes( $this->attributes );

		return Node::of( 'block', <<<HTML
		{$leading}<{$name}{$attributes}/>
		HTML );
	}

}
