<?php
namespace Fractux;

/**
 * Transforms an object into its wire representation.
 */
interface IEncodable {
	/**
	 * Become data to be transmitted.
	 *
	 * @return string
	 */
	function encode();
}
