<?php
namespace Fractux;

/**
 * Encrypted data and nonce.
 */
interface IEncryptedData {
	/**
	 * @return string
	 */
	function getNonce();

	/**
	 * @return string
	 */
	function getEnvelope();
}
