<?php
namespace Fractux;

/**
 * Signing and stuff.
 */
interface IKeystore {
	/**
	 * @return string
	 */
	function getPublicKey();

	/**
	 * @return string
	 */
	function getClientPublicKey();

	/**
	 * @param string $content
	 * @return string
	 */
	function signContent( $content );

	/**
	 * @param string $content
	 * @param string $signature
	 * @return bool
	 */
	function verifyContent( $content, $signature );

	/**
	 * @param IEncryptedData $content
	 * @param string $servicePublicKey
	 * @return \Throwable|string
	 */
	function decryptContent( $content, $servicePublicKey );

	/**
	 * @param IEncodable $content
	 * @param string $servicePublicKey
	 * @return \Throwable|IEncryptedData
	 */
	function encryptContent( $content, $servicePublicKey );

	/**
	 * @param string $servicePublicKey
	 * @param string $content
	 * @return IRegistrationRequest
	 */
	function signRegistration( $servicePublicKey, $content );

	/**
	 * @param string $servicePublicKey
	 * @return IServiceSession
	 */
	function createServiceSession( $servicePublicKey );

}
