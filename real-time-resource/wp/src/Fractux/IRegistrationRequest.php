<?php
namespace Fractux;

interface IRegistrationRequest extends IEncodable {
	/**
	 * @return string
	 */
	function encode();
}
