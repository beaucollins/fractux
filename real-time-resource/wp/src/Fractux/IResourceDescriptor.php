<?php
namespace Fractux;

use Throwable;

interface IResourceDescriptor {
	/**
	 * @return string
	 */
	function getUrl();

	/**
	 * @param IService $service
	 * @param IKeystore $keystore
	 * @return IRegistrationRequest
	 */
	function register( $service, $keystore );

	/**
	 * @param IService $service
	 * @param IKeystore $keystore
	 * @param string $encodedChallenge
	 * @return IEncodable|Throwable
	 */
	function createChallengeResponse( $service, $keystore, $encodedChallenge );
}
