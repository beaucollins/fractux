<?php
namespace Fractux;

use Throwable;

/**
 * Remote service API that supports the Fractux real-time service.
 */
interface IService {

	/**
	 * @return 'connected'|'unregistered'
	 */
	function getRegistrationStatus();

	/**
	 * @param IKeystore $keystore
	 * @param IResourceDescriptor $descriptor
	 * @return IRegistration|Throwable
	 */
	function registerResource( $keystore, $descriptor );

	/**
	 * The unique identifier for the service.
	 *
	 * @return string
	 */
	function getGUID();

	/**
	 * @return string
	 */
	function getRealtimeURI();

	/**
	 * Public key to be used in key operations.
	 * @return string
	 */
	function getPublicKey();

	/**
	 * @param IKeystore $keystore
	 * @param IResourceDescriptor $descriptor
	 * @param string $encodedChallenge
	 * @return IEncodable|Throwable
	 */
	function createChallengeResponse( $keystore, $descriptor, $encodedChallenge );

	/**
	 * @param IKeystore $keystore
	 * @param IResourceDescriptor $descriptor
	 * @param IMessage $message
	 * @return IEncodable|Throwable
	 */
	function sendMessage( $keystore, $descriptor, $message );
}
