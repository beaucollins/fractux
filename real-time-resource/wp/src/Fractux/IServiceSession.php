<?php
namespace Fractux;

interface IServiceSession {

	/**
	 * @return string
	 */
	function getPublicKey();

	/**
	 * @param string $data
	 * @return IEncryptedData
	 */
	function encrypt( $data );

	/**
	 * @param IEncryptedData $encrypted
	 * @return string
	 */
	function decrypt( $encrypted );

}
