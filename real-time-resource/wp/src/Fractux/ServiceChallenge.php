<?php
namespace Fractux;

use Fractux\Dev\SodiumSession;
use RuntimeException;
use stdClass;
use Throwable;

final class ServiceChallenge implements IEncryptedData {
	/**
	 * @param string $encodedChallenge
	 * @return IEncryptedData|Throwable
	 */
	public static function decode( $encodedChallenge ) {
		/**
		 * @var mixed
		 */
		$decoded = json_decode( $encodedChallenge );
		$nonce = SodiumSession::fetchProperty( $decoded, 'nonce' );
		$envelope = SodiumSession::fetchProperty( $decoded, 'envelope' );

		if ( $nonce instanceof Throwable ) {
			return $nonce;
		}

		if ( $envelope instanceof Throwable ) {
			return $envelope;
		}

		return new self( $nonce, $envelope );
	}

	/**
	 * @var string
	 */
	private $nonce;

	/**
	 * @var string
	 */
	private $envelope;

	/**
	 * @param string $nonce
	 * @param string $envelope
	 */
	private function __construct( $nonce, $envelope ) {
		$this->nonce = $nonce;
		$this->envelope = $envelope;
	}

	/**
	 * @return string
	 */
	public function getNonce() {
		return $this->nonce;
	}

	/**
	 * @return string
	 */
	public function getEnvelope() {
		return $this->envelope;
	}
}
