<?php
namespace Fractux\Status;

use Fractux\App\{Route, IResponse, IRequest};
use Fractux\{System, IService};

/**
 * @template S of IService
 * @template T
 * @template I
 * @template O
 *
 * @implements ISystemHandler<S,T,I,O>
 */
final class Action implements ISystemHandler {

	/**
	 * @var closure(System<S>, T, I):O
	 */
	private $action;

	/**
	 * @param closure(System<S>, T, I):O $action
	 */
	function __construct( $action ) {
		$this->action = $action;
	}

	/**
	 * @param System<S> $system
	 * @param T $value
	 * @param I $request
	 * @return O
	 */
	function serve( $system, $value, $request ) {
		$action = $this->action;
		return $action( $system, $value, $request );
	}
};
