<?php
namespace Fractux\Status;

use Fractux\App\{IHandler, IRequest, IResponse};
use Fractux\System;
use Fractux\Dev\DevService;
use Throwable;

/**
 * @template T
 * @template I
 * @template O
 *
 * @implements IHandler<T,I,O>
 */
final class DevSystemHandler implements IHandler {

	/**
	 * @template V
	 * @param closure(System<DevService>, V, IRequest):IResponse $action
	 * @return IHandler<V,IRequest,IResponse>
	 */
	public static function respond( $action ) {
		/**
		 * @var ISystemHandler<DevService,V,IRequest,IResponse>
		 */
		$handler = new Action( $action );
		return new self( $handler );
	}

	/**
	 * @var ISystemHandler<DevService,T,I,O>
	 */
	private $handler;

	/**
	 * @param ISystemHandler<DevService,T,I,O> $handler
	 */
	function __construct( $handler ) {
		$this->handler = $handler;
	}

	/**
	 * @param T $value
	 * @param I $request
	 */
	function serve( $value, $request ) {
		$system = Status::getSystem();

		if ( $system instanceof Throwable ) {
			throw $system;
		}
		return $this->handler->serve( $system, $value, $request );
	}
}
