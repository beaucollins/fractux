<?php
namespace Fractux\Status;

use Fractux\{System, IService};

/**
 * @template S of IService
 * @template V
 * @template I
 * @template O
 */
interface ISystemHandler {
	/**
	 * @param System<S> $system
	 * @param V $value
	 * @param I $request
	 * @return O
	 */
	function serve( $system, $value, $request );
}
