<?php
namespace Fractux\Status;

use Fractux\{System, IResourceDescriptor, IMessage, IEncodable};
use Fractux\Dev\{DevService, WebhookResource};
use Fractux\App\{Route, View, Path};
use function Fractux\App\{route, prefix, combine, get, getPath, post, postPath };
use Fractux\App\{IMatcher, IRequest, IResponse };
use function Fractux\Html\{ div, body };

final class Routes {

	/**
	 * @return IMatcher<IRequest,IResponse>
	 */
	public static function mount() {
		return combine(
			route(
				getPath( '/' ), DevSystemHandler::respond(
					/**
					 * @param System<DevService> $system
					 * @param string $path
					 * @param IRequest $request
					 */
					function( $system, $path, $request ) {
						return View::staticResponse( Views::status( $system ) );
					}
				)
			),
			route(
				postPath( '/' ), DevSystemHandler::respond(
					function( $system, string $path, $request ) {
						return View::jsonResponse(
							[
								'guid' => $system->getService()->getGUID(),
								'path' => $path,
								'requested' => $request->getRequestedPath(),
							]
						);
					}
				)
			),
			route(
				postPath( '/connect' ), DevSystemHandler::respond(
					function( $system, string $path, $request ) {
						$registration = $system->registerResource( self::resourceDescriptor() );
						if ( $registration instanceof \Throwable ) {
							return View::jsonResponse( [ 'message' => $registration->getMessage() ], 400 );
						}
						return View::staticResponse( $registration->encode(), 200 );
					}
				)
			),
			route(
				postPath( '/register' ), DevSystemHandler::respond(
					function( $system, string $path, $request ) {
						$challengeResponse = $system->createChallengeResponse( self::resourceDescriptor(), file_get_contents( 'php://input' ) );
						if ( $challengeResponse instanceof \Throwable ) {
							return View::jsonResponse( ['status' => 'error', 'message' => $challengeResponse->getMessage() ], 400 );
						}
						return View::staticResponse( $challengeResponse->encode(), 200 );
					}
				)
			),
			route(
				postPath( '/test' ), DevSystemHandler::respond(
					function( $system ) {
						$response = $system->sendMessage( self::resourceDescriptor(), self::message( 'admin', self::jsonEncoded( [ 'type' => 'ping' ] ) ) );
						if ( $response instanceof \Throwable ) {
							return View::jsonResponse( ['status' => 'error', 'message' => $response->getMessage() ], 400 );
						}
						return View::staticResponse( $response->encode(), 200 );
					}
				)
			),
			prefix(
				'/_internal', combine(
					route(
						get( Path::like( '/add/:a/:b' ) ),
						View::respond(
							/**
							 * @param array<string,?string> $params
							 * @param IRequest $request
							 * @psalm-suppress UnusedClosureParam
							 */
							function( $params, $request ) {
								$a = $params['a'];
								$b = $params['b'];

								if ( ! is_numeric( $a ) || ! is_numeric( $b ) ) {
									return Views::template( 'Error', body( [], 'Can only add numbers' ) );
								}
								$sum = floatval( $a ) + floatval( $b );
								return Views::template( 'Add', body( [], div( [], 'Sum: ' . $sum ) ) );
							}
						)
					),
					route( getPath( '/' ), View::static( 'internal1' ) ),
					...array_map(
						function( $num ) {
							if ( is_array( $num ) ) {
								return route( getPath( "/{$num[0]}" ), View::static( div( [], (string) $num[1] ) ) );
							}
							return combine(
								route( getPath( "/{$num}" ), View::static( Views::template( "The Number: $num", div( [], $num ) ) ) ),
								route( getPath( "/{$num}/reverse" ), View::static( Views::template( "Reverse: {$num}", div( [], strrev( $num ) ) ) ) )
							);
						},
						[ [ 'one', 1 ], 'two', 'three', 'four', 'five', 'six', 'ten' ]
					)
				)
			)
		);
	}

	/**
	 * @return IResourceDescriptor
	 */
	static function resourceDescriptor() {
		$resource = WebhookResource::create( getenv( 'RESOURCE_WEBHOOK_URI', true ) );

		if ( $resource instanceof \Throwable ) {
			throw $resource;
		}
		return $resource;
	}

	/**
	 * @param string $topic
	 * @param IEncodable $payload
	 * @return IMessage
	 */
	static function message( $topic, $payload ) {
		return new class( $payload ) implements IMessage {
			/**
			 * @var IEncodable
			 */
			private $payload;

			/**
			 * @param IEncodable $payload
			 */
			function __construct( $payload ) {
				$this->payload = $payload;
			}

			function encode() {
				return $this->payload->encode();
			}
		};
	}

	/**
	 * @param mixed $value
	 * @return IEncodable
	 */
	static function jsonEncoded( $value ) {
		return new class( $value ) implements IEncodable {
			/**
			 * @var mixed
			 */
			private $value;

			/**
			 * @param mixed $value
			 */
			function __construct( $value ) {
				$this->value = $value;
			}

			function encode() {
				return json_encode( $this->value );
			}
		};
	}
}
