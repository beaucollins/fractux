<?php
namespace Fractux\Status;

use Fractux\App\{IHandler, IRequest, IResponse};
use Fractux\{System, IKeystore};

use Fractux\Dev\{DevService, FileKeystore};

use Throwable;

final class Status {

	/**
	 * @return System<DevService>|Throwable
	 */
	public static function getSystem() {
		/**
		 * @var System<DevService>|Throwable
		 */
		$system = System::initialize(
			self::getService(),
			self::getKeystore()
		);
		return $system;
	}

	/**
	 * @return DevService|Throwable
	 */
	public static function getService() {
		return DevService::createService( 'http://localhost:5433' );
	}

	/**
	 * @return IKeystore|Throwable
	 */
	public static function getKeystore() {
		return FileKeystore::createKeystore( dirname( getcwd() ) . '/data/keys' );
	}

	/**
	 * @template T
	 * @param ISystemHandler<DevService, T,IRequest,IResponse> $handler
	 * @return IHandler<T,IRequest,IResponse>
	 */
	public static function systemView( $handler ) {
		return new DevSystemHandler( $handler );
	}
}
