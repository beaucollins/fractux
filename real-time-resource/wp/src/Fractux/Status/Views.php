<?php
namespace Fractux\Status;

use Fractux\Html\IDomElement;
use Fractux\{System, IService};
use function Fractux\Html\{
	document, html, body, head, safe, element,
	div, title, script, section, hr, span,
	fragment, stylesheet, a, meta
};

final class Views {
	/**
	 * @param System $system
	 * @return IDomElement
	 */
	static function status( $system ) {
		return self::template(
			'System Status',
			fragment(
				script( [ 'src' => '/js/status.js' ] ),
				script(
					[],
					<<<JS
						window.fractux('{$system->getService()->getRealtimeURI()}');
					JS
				)
			),
			body( [],
				self::info( 'Host', gethostname() ),
				self::info( 'Resource Public Key', sodium_bin2hex( $system->getClientPublicKey() ) ),
				self::info( 'Service Public Key', sodium_bin2hex( $system->getService()->getPublicKey() ) ),
				self::info( 'Service GUID', $system->getService()->getGUID() ),
				self::info( 'Registration Status', self::serviceStatus( $system->getService() ) ),
			)
		);
	}

	/**
	 * @param string $label
	 * @param string|IDomElement $value
	 * @return IDomElement
	 */
	static function info( $label, $value ) {
		return (
			div( [ 'class' => 'system-info' ],
				$label,
				hr( [] ),
				$value,
			)
		);
	}

	/**
	 * @param string $title
	 * @param IDomElement $head
	 * @param IDomElement $body
	 * @return IDomElement
	 */
	static function template( $title, $head, $body = null ) {
		if ( $body === null ) {
			$body = $head;
			$head = fragment( [] );
		}
		return (
			document(
				html( [ 'lang' => 'en-us' ],
					head( [],
						meta( [ 'charset' => 'utf-8' ] ),
						title( [], $title ),
						$head,
						stylesheet( '/css/style.css' ),
					),
					$body,
				),
			)
		);
	}

	/**
	 * @param IService $service
	 * @return IDomElement
	 */
	static function serviceStatus( $service ) {
		$status = $service->getRegistrationStatus();
		switch ( $status ) {
			case 'unregistered':
				$button = a( [ 'href' => '#register' ], 'Register Service' );
				break;

			case 'connected':
				$button = a( [ 'href' => '#test' ], 'Test Service' );
				break;

			// default:
			// 	$button = '';
		}
		return span( [ 'class' => 'status-unknown' ],
			$status,
			' ',
			$button
		);
	}
}
