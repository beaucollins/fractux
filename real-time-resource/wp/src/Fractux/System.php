<?php
namespace Fractux;

use Throwable;

/**
 * Where it all comes together.
 *
 * To particpate in the Fractux real-time service, a resource needs to
 * perform a combination of cryptographic and ACL type tasks.
 *
 * The end user of the Fractux system is a web connected human or machine
 * that is requesting access to a resource. A resource publishes and subscribes to a channel
 * in the Fractux service.
 *
 * The Fractux real-time service uses public key cryptography to verify that
 * 1) Users can subscribe to a resource
 * 2) Users can publish to a resource
 *
 * A Fractux resource is _also_ a Fractux user. It technically doesn't need
 * to be however to provide any useful end features this is almost always the case.
 *
 * When a user wants to subscribe to a fractux resource, they provide a token containing
 *  identity: User identifying information
 *  resourceDescriptor: Resource subscription identifier in the form of a URL
 *
 * The `identity` is opaque to the Fractux service. It passes that information to the service
 * as is.
 *
 * The `resourceDescriptor` is how Fractux attempts to communicate with the resource.
 *
 * @template T of IService
 */
final class System {
	/**
	 * @template S of IService
	 * @param S|Throwable $service
	 * @param IKeystore|Throwable $keystore
	 * @return System<S>|Throwable
	 */
	public static function initialize( $service, $keystore ) {
		if ( $service instanceof Throwable ) {
			return $service;
		}

		if ( $keystore instanceof Throwable ) {
			return $keystore;
		}
		return new self( $service, $keystore );
	}

	/**
	 * @var IKeystore
	 */
	private $keystore;

	/**
	 * @var T
	 */
	private $service;

	/**
	 * @param T $service
	 * @param IKeystore $keystore
	 */
	private function __construct( $service, $keystore ) {
		$this->service = $service;
		$this->keystore = $keystore;
	}

	/**
	 * @return T
	 */
	public function getService() {
		return $this->service;
	}

	/**
	 * @return string
	 */
	public function getClientPublicKey() {
		return $this->keystore->getClientPublicKey();
	}

	/**
	 * @return string
	 */
	public function getPublicKey() {
		return $this->keystore->getPublicKey();
	}

	/**
	 * Requests registration with a Fractux registry.
	 *
	 * This will be synchoronous in the eyes of the PHP process even though
	 * the service itself will be making a request to the resource descriptor
	 * to complete key exchange business.
	 *
	 * @param IResourceDescriptor $descriptor
	 * @return IRegistration|Throwable
	 */
	public function registerResource( $descriptor ) {
		/**
		 * Let's start the dance now.
		 */
		return $this->service->registerResource( $this->keystore, $descriptor );
	}

	/**
	 * @param IResourceDescriptor $descriptor
	 * @param string $encoded
	 * @return IEncodable|Throwable
	 */
	public function createChallengeResponse( $descriptor, $encoded ) {
		return $this->service->createChallengeResponse( $this->keystore, $descriptor, $encoded );
	}

	/**
	 * Send a real-time message out via the service connected to the resource descriptor
	 *
	 * @param IResourceDescriptor $descriptor
	 * @param IMessage $message
	 * @return IEncodable|Throwable
	 */
	public function sendMessage( $descriptor, $message ) {
		return $this->service->sendMessage( $this->keystore, $descriptor, $message );
	}
}
