<?php
namespace Fractux\Test;

use Fractux\Dev\SodiumKeystore;
use Fractux\IKeystore;

final class MemoryKeystore extends SodiumKeystore {

	/**
	 * @param string $seed
	 * @return IKeystore
	 */
	public static function seed( $seed ) {
		return new self( $seed );
	}

	/**
	 * @var string
	 */
	private $keypair;

	/**
	 * @param string $keypair
	 * @psalm-suppress UnusedParam
	 */
	private function __construct( $keypair ) {
		$this->keypair = sodium_crypto_sign_keypair();
	}

	/**
	 * @return string
	 */
	public function getKeyPair() {
		return $this->keypair;
	}

}
