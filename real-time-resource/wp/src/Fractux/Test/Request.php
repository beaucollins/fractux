<?php
namespace Fractux\Test;

use Fractux\App\{IRequest, IUnmatched, IMatcher};

final class Request {

	/**
	 * @throws \UnexpectedValueException
	 *
	 * @template T
	 * @param IRequest $request
	 * @param IMatcher<IRequest,T> $matcher
	 * @return T
	 */
	public static function request( $request, $matcher ) {
		$result = $matcher->matches( $request );
		if ( $result instanceof IUnmatched ) {
			throw new \UnexpectedValueException( 'Route did not match request: ' . self::inspectRequest( $request ) );
		}
		return $result;
	}

	/**
	 * @param IRequest $request
	 * @return string
	 */
	public static function inspectRequest( $request ) {
		return sprintf( '%s %s', $request->getMethod(), $request->getPath() );
	}

	/**
	 * @param string $method
	 * @param string $path
	 * @param string $query
	 * @param mixed $body
	 * @return IRequest
	 */
	public static function buildRequest( $method, $path, $query = '', $body = null ): IRequest {
		return new class( $method, $path, $query, $body ) implements IRequest {
			private string $path;
			private string $method;
			private string $query;
			/**
			 * @var mixed
			 */
			private $body;
			/**
			 * @param string $method
			 * @param string $path
			 * @param string $query
			 * @param mixed $body
			 */
			function __construct( string $method, string $path, string $query, $body = null ) {
				$this->path = $path;
				$this->method = $method;
				$this->query = $query;
				$this->body = $body;
			}

			public function getPath() {
				return $this->path;
			}

			public function getMethod() {
				return $this->method;
			}

			public function getRequestedPath() {
				return $this->path;
			}

			public function getQuery() {
				return $this->query;
			}

			public function getBody() {
				return $this->body;
			}

			public function getHeaders() {
				return [];
			}
		};
	}
}
