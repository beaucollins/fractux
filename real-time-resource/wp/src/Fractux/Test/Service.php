<?php
namespace Fractux\Test;

use Fractux\{IService, IKeystore, IRegistration, IChallenge, IEncryptedData, IServiceRegistrationStatus};
use Fractux\Dev\SodiumSession;

use stdClass;
use RuntimeException;
use Throwable;

final class Service implements IService {

	/**
	 * @var string
	 */
	private $keypair;

	function __construct() {
		$this->keypair = sodium_crypto_kx_seed_keypair(
			sodium_pad( 'super-secure', SODIUM_CRYPTO_KX_SEEDBYTES )
		);
	}

	public function getGUID() {
		return 'http://test.local';
	}

	public function getRealtimeURI() {
		return 'ws://test.local/resource';
	}

	/**
	 * @inheritDoc
	 */
	public function getRegistrationStatus() {
		return 'unregistered';
	}

	/**
	 * @return string
	 */
	public function getKeypair() {
		return $this->keypair;
	}

	public function getPublicKey() {
		return sodium_crypto_kx_publickey( $this->keypair );
	}

	public function registerResource( $keystore, $descriptor ) {
		$request = $descriptor->register( $this, $keystore )->encode();

		// 0) Decode the message
		$registration = self::decodeRegistrationRequest( $request );

		if ( $registration instanceof Throwable ) {
			return $registration;
		}

		// 1) Create the crypto kx session
		$session = SodiumSession::createServerSession( $registration->getPublicKey(), $this->keypair );

		// 2) Decrypt tho configuration
		$plain = $session->decrypt( $registration );

		if ( ! $plain ) {
			return new RuntimeException( 'Unable to decrypt' );
		}

		// 3) This is where the service will send a challenge to the described resource
		// For development/test purposes we aren't doing that here
		return new class () implements IRegistration {
			function encode() {
				return '';
			}
		};
	}

	/**
	 * @inheritDoc
	 */
	public function createChallengeResponse( $keystore, $descriptor, $encodedChallenge ) {
		return $descriptor->createChallengeResponse( $this, $keystore, $encodedChallenge );
	}

	/**
	 * @inheritDoc
	 */
	public function sendMessage( $keystore, $descriptor, $message ) {
		throw new \RuntimeException( 'Not implemented ' . __FUNCTION__ );
	}

	/**
	 * Returns Server session keys for the given client.
	 *
	 * [rx, tx]
	 *
	 * @param string $clientPublicKey
	 * @return Array<string>
	 */
	private function createKXKeypair( $clientPublicKey ) {
		return sodium_crypto_kx_server_session_keys( $this->keypair, $clientPublicKey );
	}

	/**
	 * @param mixed $encoded
	 * @return string|Throwable
	 */
	private static function decodeHex( $encoded ) {
		if ( ! is_string( $encoded ) ) {
			return new RuntimeException( 'invalid dada' );
		}
		try {
			return sodium_hex2bin( $encoded );
		} catch ( Throwable $exception ) {
			return $exception;
		}
	}

	/**
	 * @param string $encoded
	 * @return RegistrationRequest|Throwable
	 */
	public static function decodeRegistrationRequest( $encoded ) {
		/**
		 * @var mixed
		 */
		$decoded = json_decode( $encoded, false );

		if ( ! $decoded instanceof stdClass ) {
			return new RuntimeException( 'Unable to decode.' );
		}

		$bin = self::decodeHex( SodiumSession::fetchProperty( $decoded, 'publicKey' ) );

		if ( $bin instanceof Throwable ) {
			return $bin;
		}

		$nonce = SodiumSession::fetchProperty( $decoded, 'nonce' );
		$decoded = SodiumSession::fetchProperty( $decoded, 'envelope' );

		if ( $nonce instanceof Throwable ) {
			return $nonce;
		}

		if ( $decoded instanceof Throwable ) {
			return $decoded;
		}

		return new RegistrationRequest(
			$bin,
			$nonce,
			$decoded
		);
	}

	/**
	 * @param string $encoded
	 * @return IEncryptedData|Throwable
	 */
	public static function decodeEncrypted( $encoded ) {
		return SodiumSession::decode( $encoded );
	}
}

class RegistrationRequest implements IEncryptedData {

	/**
	 * @var string
	 */
	private $publicKey;

	/**
	 * @var string
	 */
	private $nonce;

	/**
	 * @var string
	 */
	private $envelope;

	/**
	 * @param string $publicKey
	 * @param string $nonce
	 * @param string $envelope
	 */
	function __construct( $publicKey, $nonce, $envelope ) {
		$this->publicKey = $publicKey;
		$this->nonce = $nonce;
		$this->envelope = $envelope;
	}

	/**
	 * @return string
	 */
	function getPublicKey() {
		return $this->publicKey;
	}

	function getNonce() {
		return $this->nonce;
	}

	function getEnvelope() {
		return $this->envelope;
	}
}
