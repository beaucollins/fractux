<?php
namespace Fractux\App;

/**
 * @template V
 * @param IMatcher<IRequest,V> $matcher
 * @param IHandler<V,IRequest,IResponse> $handler
 * @return IMatcher<IRequest,IResponse>
 */
function route( $matcher, $handler ) {
	return Route::mount( $matcher, $handler );
}

/**
 * @template T
 * @param IMatcher<string,T> $detectPath
 * @return IMatcher<IRequest,T>
 */
function get( $detectPath ) {
	return Route::get( $detectPath );
}

/**
 * @param string $path
 * @return IMatcher<IRequest, string>
 */
function getPath( $path ) {
	return Route::get( Path::is( $path ) );
}

/**
 * @template T
 * @param IMatcher<string,T> $detectPath
 * @return IMatcher<IRequest,T>
 */
function post( $detectPath ) {
	return Route::post( $detectPath );
}

/**
 * @param string $path
 * @return IMatcher<IRequest, string>
 */
function postPath( $path ) {
	return Route::post( Path::is( $path ) );
}

/**
 * @param 'GET'|'POST' $method
 * @param string $path
 * @return IMatcher<IRequest, string>
 */
function requestedPath( $method, $path ) {
	return Route::requestPath( $method, Path::is( $path ) );
}

/**
 * @param IMatcher<IRequest, IResponse> ...$routes
 * @return IMatcher<IRequest, IResponse>
 */
function combine( ...$routes ) {
	return Route::combine( ...$routes );
}

/**
 * @template TResponse of IResponse
 * @param string $prefix
 * @param IMatcher<IRequest,TResponse> $route
 * @return IMatcher<IRequest,TResponse>
 */
function prefix( $prefix, $route ) {
	return Route::prefix( $prefix, $route );
}
