<?php
namespace Fractux\Html;

use Fractux\Html\Html;

/**
 * @param string $elementName
 * @param array<string,string> $attributes
 * @param string|IDomElement|array<string|IDomElement> ...$children
 * @return IDomElement
 */
function element( $elementName, $attributes, ...$children ) {
	return Html::element( $elementName, $attributes, ...$children );
}

/**
 * @param array<string,string> $attributes
 * @param string|IDomElement|array<string|IDomElement> ...$children
 * @return IDomElement
 */
function div( $attributes, ...$children ) {
	return Html::element( 'div', $attributes, ...$children );
}

/**
 * @param mixed $content
 * @return IDomElement
 */
function unsafe( $content ) {
	return Html::unsafe( $content );
}

/**
 * @param array<string,string> $attributes
 * @param string|IDomElement|array<string|IDomElement> ...$children
 * @return IDomElement
 */
function body( $attributes, ...$children ) {
	return Html::element( 'body', $attributes, ...$children );
}

/**
 * @param string|IDomElement $html
 * @return IDomElement
 */
function document( $html ) {
	return Html::document( $html );
}

/**
 * @param array<IDomElement|string>|IDomElement|string ...$children
 * @return IDomElement
 */
function fragment( ...$children ) {
	return Fragment::from( $children );
}

/**
 * @param array<string,string> $attributes
 * @param string|IDomElement|array<string|IDomElement> ...$children
 * @return IDomElement
 */
function html( $attributes, ...$children ) {
	return Html::element( 'html', $attributes, ...$children );
}

/**
 * @param array<string,string> $attributes
 * @param string|IDomElement|array<string|IDomElement> ...$children
 * @return IDomElement
 */
function head( $attributes, ...$children ) {
	return Html::element( 'head', $attributes, ...$children );
}

/**
 * @param array<string,string> $attributes
 * @param string|IDomElement|array<string|IDomElement> ...$children
 * @return IDomElement
 */
function title( $attributes, ...$children ) {
	return Html::element( 'title', $attributes, ...$children );
}

/**
 * @param array<string,string> $attributes
 * @param string|IDomElement|array<string|IDomElement> ...$children
 * @return IDomElement
 */
function table( $attributes, ...$children ) {
	return Html::element( 'table', $attributes, ...$children );
}

/**
 * @param array<string,string> $attributes
 * @param string|IDomElement|array<string|IDomElement> ...$children
 * @return IDomElement
 */
function tr( $attributes, ...$children ) {
	return Html::element( 'tr', $attributes, ...$children );
}

/**
 * @param array<string,string> $attributes
 * @param string|IDomElement|array<string|IDomElement> ...$children
 * @return IDomElement
 */
function td( $attributes, ...$children ) {
	return Html::element( 'td', $attributes, ...$children );
}

/**
 * @param array<string,string> $attributes
 * @param string|IDomElement|array<string|IDomElement> ...$children
 * @return IDomElement
 */
function tbody( $attributes, ...$children ) {
	return Html::element( 'tbody', $attributes, ...$children );
}

/**
 * @param array<string, string> $attributes
 * @param string|IDomElement|array<string|IDomElement> $children
 * @return IDomElement
 */
function th( $attributes, ...$children ) {
	return Html::element( 'th', $attributes, ...$children );
}

/**
 * @param array<string, string> $attributes
 * @param string|IDomElement|array<string|IDomElement> $children
 * @return IDomElement
 */
function script( $attributes, ...$children ) {
	return Html::element( 'script', $attributes, ...$children );
}

/**
 * @param array<string,string> $attributes
 * @return IDomElement
 */
function link( $attributes ) {
	return new SelfClosingElement( 'link', $attributes );
}

/**
 * @param array<string,string> $attributes
 * @return IDomElement
 */
function hr( $attributes ) {
	return new SelfClosingElement( 'hr', $attributes );
}

/**
 * @param array<string,string> $attributes
 * @param string|IDomElement|array<string|IDomElement> ...$children
 * @return IDomElement
 */
function section( $attributes, ...$children ) {
	return Html::element( 'section', $attributes, ...$children );
}

/**
 * @param array<string,string> $attributes
 * @param string|IDomElement|array<string|IDomElement> ...$children
 * @return IDomElement
 */
function header( $attributes, ...$children ) {
	return Html::element( 'header', $attributes, ...$children );
}

/**
 * @param array<string,string> $attributes
 * @param string|IDomElement|array<string|IDomElement> ...$children
 * @return IDomElement
 */
function h1( $attributes, ...$children ) {
	return Html::element( 'h1', $attributes, ...$children );
}

/**
 * @param array<string,string> $attributes
 * @param string|IDomElement|array<string|IDomElement> ...$children
 * @return IDomElement
 */
function h2( $attributes, ...$children ) {
	return Html::element( 'h2', $attributes, ...$children );
}

/**
 * @param array<string,string> $attributes
 * @param string|IDomElement|array<string|IDomElement> ...$children
 * @return IDomElement
 */
function h3( $attributes, ...$children ) {
	return Html::element( 'h3', $attributes, ...$children );
}

/**
 * @param array<string,string> $attributes
 * @param string|IDomElement|array<string|IDomElement> ...$children
 * @return IDomElement
 */
function h4( $attributes, ...$children ) {
	return Html::element( 'h4', $attributes, ...$children );
}

/**
 * @param array<string,string> $attributes
 * @param string|IDomElement|array<string|IDomElement> ...$children
 * @return IDomElement
 */
function h5( $attributes, ...$children ) {
	return Html::element( 'h5', $attributes, ...$children );
}

/**
 * @param array<string,string> $attributes
 * @param string|IDomElement|array<string|IDomElement> ...$children
 * @return IDomElement
 */
function p( $attributes, ...$children ) {
	return Html::element( 'p', $attributes, ...$children );
}

/**
 * @param array<string,string> $attributes
 * @param string|IDomElement|array<string|IDomElement> ...$children
 * @return IDomElement
 */
function pre( $attributes, ...$children ) {
	return Html::element( 'pre', $attributes, ...$children );
}

/**
 * @param array<string,string> $attributes
 * @param string|IDomElement|array<string|IDomElement> ...$children
 * @return IDomElement
 */
function ul( $attributes, ...$children ) {
	return Html::element( 'ul', $attributes, ...$children );
}

/**
 * @param array<string,string> $attributes
 * @param string|IDomElement|array<string|IDomElement> ...$children
 * @return IDomElement
 */
function ol( $attributes, ...$children ) {
	return Html::element( 'ol', $attributes, ...$children );
}

/**
 * @param array<string,string> $attributes
 * @param string|IDomElement|array<string|IDomElement> ...$children
 * @return IDomElement
 */
function li( $attributes, ...$children ) {
	return Html::element( 'li', $attributes, ...$children );
}

/**
 * @param array<string,string> $attributes
 * @param string|IDomElement|array<string|IDomElement> ...$children
 * @return IDomElement
 */
function span( $attributes, ...$children ) {
	return Html::element( 'span', $attributes, ...$children );
}

/**
 * @param array<string,string> $attributes
 * @param string|IDomElement|array<string|IDomElement> ...$children
 * @return IDomElement
 */
function em( $attributes, ...$children ) {
	return Html::element( 'em', $attributes, ...$children );
}

/**
 * @param array<string,string> $attributes
 * @param string|IDomElement|array<string|IDomElement> ...$children
 * @return IDomElement
 */
function a( $attributes, ...$children ) {
	return Html::element( 'a', $attributes, ...$children );
}

/**
 * @param array<string,string> $attributes
 * @return IDomElement
 */
function meta( $attributes ) {
	return new SelfClosingElement( 'meta', $attributes );
}

/**
 * @param string $href
 * @param string $title
 * @return IDomElement
 */
function stylesheet( $href, $title = null ) {
	$atts = [
		'type' => 'text/css',
		'rel' => 'stylesheet',
		'href' => $href,
	];
	if ( $title !== null ) {
		$atts['title'] = $title;
	}
	return link( $atts );
}
