<?php
namespace Fractux\App;

final class AppTest extends \PHPUnit\Framework\TestCase {

	public function testHomePage(): void {
		$response = self::request( 'GET', '/' );

		$this->assertEquals( 200, $response->getStatus() );
		$this->assertStringContainsString( '<title>Fractux CRE</title>', $response->getBody() );
		$this->assertEquals( [], $response->getHeaders() );
	}

	public function testNotFound(): void {
		$response = self::request( 'GET', '/not-a-page' );
		$this->assertEquals( 404, $response->getStatus() );
		$this->assertStringContainsString( 'Fractux\App\RouteNotFound', $response->getBody() );
	}

	public function testRedirect(): void {
		$response = self::request( 'POST', '/hello', [ 'name' => 'sam' ] );
		$this->assertEquals( 302, $response->getStatus() );
		$this->assertEquals( [ 'Location' => '/hello/sam' ], $response->getHeaders() );
		$this->assertEquals( '', $response->getBody() );
	}

	public function testGetHelloName(): void {
		$response = self::request( 'GET', '/hello/sam' );
		$this->assertEquals( 200, $response->getStatus() );
		$this->assertStringContainsString( '<div>Hello, sam.</div>', $response->getBody() );
	}

	public function testPost(): void {
		$matched = post( Path::is( '/something-special' ) )->matches( \Fractux\Test\Request::buildRequest( 'POST', '/something-special' ) );
		$unmatched = post( Path::is( '/something-special' ) )->matches( \Fractux\Test\Request::buildRequest( 'GET', '/something-special' ) );

		$this->assertEquals( '/something-special', $matched );
		$this->assertInstanceOf( IUnmatched::class, $unmatched );
	}

	public function testRequestedPath(): void {
		$matched = requestedPath( 'GET', '/other-thing' )->matches( \Fractux\Test\Request::buildRequest( 'GET', '/other-thing' ) );

		$this->assertEquals( '/other-thing', $matched );
	}

	/**
	 * @psalm-param mixed $body
	 */
	private static function request( string $method, string $path, $body = null ): IResponse {
		return App::mount()->serve( \Fractux\Test\Request::buildRequest( $method, $path, '', $body ) );
	}

}
