<?php
namespace Fractux\App;

use Fractux\Test\Request;

final class MatcherMapTest extends \PHPUnit\Framework\TestCase {

	public function testMapped(): void {
		$thing = new \RuntimeException( 'hi' );
		$matcher = getPath( '/special-path' );

		$mapper = MatcherMap::map(
			$matcher,
			function() use ( $thing ) {
				return $thing;
			}
		);

		$result = $matcher->matches( Request::buildRequest( 'GET', '/special-path' ) );

		$this->assertEquals( '/special-path', $result );

		$this->assertInstanceOf(
			IUnmatched::class,
			$mapper->matches( Request::buildRequest( 'GET', '/' ) )
		);

		$this->assertEquals(
			$thing,
			$mapper->matches( Request::buildRequest( 'GET', '/special-path' ) )
		);
	}
}
