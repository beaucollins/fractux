<?php
namespace Fractux\App;

class PathTest extends \PHPUnit\Framework\TestCase {

	public function testPathCompile(): void {
		$this->assertEquals(
			[ 'name' => 'carl' ],
			Path::compile( '/hello/:name', '/hello/carl' )
		);

		$this->assertEquals(
			[
				'name' => 'Gina',
				'other' => 'cats',
			],
			Path::compile( '/hello/:name/and/the/:other', '/hello/Gina/and/the/cats' )
		);

		$this->assertEquals(
			Route::unmatched(),
			Path::compile( '/some-kind-of/:thing/is-here', '/some-kind-of/dog' )
		);

		$this->assertEquals(
			Route::unmatched(),
			Path::compile( '/some-kind-of/:thing', '/some-kind-of/dog/is-here' )
		);

	}

	public function testBadTemplate(): void {
		$this->assertEquals(
			Route::unmatched(),
			Path::compile( '/a/:/b', '/a/c/b' )
		);
	}
}
