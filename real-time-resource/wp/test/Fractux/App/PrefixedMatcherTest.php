<?php
namespace Fractux\App;

use Fractux\Test\Request;

final class PrefixedMatcherTest extends \PHPUnit\Framework\TestCase {

	public function testPrefixedMatch(): void {
		$request = Request::buildRequest( 'GET', '/hello/there' );

		/**
		 * @var IMatcher<IRequest,PrefixedMatcherTest\Response>
		 */
		$route = Route::match(
			/**
			 * @param IRequest $request
			 * @return IUnmatched|PrefixedMatcherTest\Response
			 */
			function( $request ) {
				return new PrefixedMatcherTest\Response( $request );
			}
		);

		$matcher = Route::prefix( '/hello', $route );

		$matched = $matcher->matches( $request );

		if ( $matched instanceof IUnmatched ) {
			throw new \UnexpectedValueException( 'Route did not match' );
		}

		$prefixedRequest = $matched->request;
		$this->assertEquals( '/there', $prefixedRequest->getPath() );
		$this->assertEquals( '/hello/there', $prefixedRequest->getRequestedPath() );
		$this->assertEquals( '', $prefixedRequest->getQuery() );
	}

}

namespace Fractux\App\PrefixedMatcherTest;

use Fractux\App\{IResponse, IRequest};

final class Response implements IResponse {
	/**
	 * @var IRequest
	 */
	public $request;

	/**
	 * @param IRequest $request
	 */
	function __construct( $request ) {
		$this->request = $request;
	}

	function getStatus() {
		return 200;
	}

	function getHeaders() {
		return [];
	}

	function getBody() {
		return '';
	}
}
