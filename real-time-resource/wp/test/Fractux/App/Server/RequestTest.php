<?php
namespace Fractux\App\Server;

final class RequestTest extends \PHPUnit\Framework\TestCase {

	/**
	 * @beforeClass
	 * @return void
	 */
	public static function copyServer() {

	}

	public function testBuildRequest(): void {
		$request = Request::buildRequest();

		$this->assertEquals( 'GET', $request->getMethod() );
	}
}

if ( ! function_exists( 'apache_request_headers' ) ) {
	/**
	 * @return array
	 */
	function apache_request_headers() {
		return [];
	}
}
