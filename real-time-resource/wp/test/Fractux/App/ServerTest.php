<?php
namespace Fractux\App;

use Throwable;
use RuntimeException;

use Fractux\Test\Request;

final class ServerTest extends \PHPUnit\Framework\TestCase {

	/**
	 * @return void
	 */
	public function testServeThrowable() {

		$request = Request::buildRequest( 'GET', '/somewhere' );
		$exception = new RuntimeException( 'This is a test' );

		$endpoint = new class( $exception ) implements IEndpoint {
			/**
			 * @var Throwable
			 */
			private $exception;

			/**
			 * @param Throwable $exception
			 */
			function __construct( $exception ) {
				$this->exception = $exception;
			}

			public function serve( $request ) {
				throw $this->exception;
			}
		};

		$response = Server::getResponse( $endpoint, $request );

		$expected = ErrorResponseHandler::document( $request, $exception );
		$this->assertEquals( 500, $response->getStatus() );
		$this->assertEquals( $expected, $response->getBody() );
	}

}
