<?php
namespace Fractux\Dev;

final class CachedFetchTest extends \PHPUnit\Framework\TestCase {

	function testIsCached(): void {
		$counter = new class() implements IFetch {
			/**
			 * @var int
			 */
			private $counter = 0;

			function fetch() {
				$this->counter += 1;
				return $this->counter;
			}
		};

		$cached = new CachedFetch( $counter );

		self::assertEquals( 1, $cached->fetch() );
		self::assertEquals( 1, $cached->fetch() );
		self::assertEquals( 2, $counter->fetch() );
	}

	function testException(): void {
		$cached = new CachedFetch(
			new class implements IFetch {
				function fetch() {
					return new \RuntimeException( 'lol' );
				}
			}
		);

		self::assertInstanceOf( \Throwable::class, $cached->fetch() );
	}

	function testNullFetch(): void {
		$cached = new CachedFetch(
			new class implements IFetch {
				function fetch() {
					return null;
				}
			}
		);

		self::assertInstanceOf( \UnexpectedValueException::class, $cached->fetch() );

	}
}
