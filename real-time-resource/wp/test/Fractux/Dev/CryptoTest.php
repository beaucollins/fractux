<?php
namespace Fractux\Dev;

final class CryptoTest extends \PHPUnit\Framework\TestCase {

	/**
	 * @return void
	 */
	public function testKeyExchange() {
		$seed = sodium_pad( 'registry.com', SODIUM_CRYPTO_KX_SEEDBYTES );
		$client = sodium_crypto_kx_seed_keypair( $seed );
		$server = sodium_crypto_kx_seed_keypair( sodium_pad( 'server', SODIUM_CRYPTO_KX_SEEDBYTES ) );

		$client_keys = sodium_crypto_kx_client_session_keys( $client, sodium_crypto_kx_publickey( $server ) );
		$server_keys = sodium_crypto_kx_server_session_keys( $server, sodium_crypto_kx_publickey( $client ) );

		$message = 'lorem ipsum';

		$nonce = random_bytes( SODIUM_CRYPTO_SECRETBOX_NONCEBYTES );
		$cipher = sodium_crypto_secretbox( $message, $nonce, $client_keys[1] );

		$plain = sodium_crypto_secretbox_open( $cipher, $nonce, $server_keys[0] );

		self::assertEquals( $message, $plain );
	}

}
