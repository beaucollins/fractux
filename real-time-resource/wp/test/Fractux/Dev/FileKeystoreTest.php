<?php
namespace Fractux\Dev;

use Throwable;

final class FileKeystoreTest extends \PHPUnit\Framework\TestCase {

	/**
	 * @before
	 * @return void
	 */
	public static function cleanKeystore() {
		$dir = dirname( self::keystorePath() );
		if ( is_dir( $dir ) ) {
			exec( sprintf( 'rm -r %s', escapeshellarg( $dir ) ) );
		}
	}

	/**
	 * @return string
	 */
	private static function keystorePath() {
		return 'test/fixtures/keystore';
	}

	/**
	 * @return void
	 */
	public function testSign() {
		$store = FileKeystore::createKeystore( self::keystorePath() );

		if ( $store instanceof Throwable ) {
			throw $store;
		}

		self::assertTrue( $store->verifyContent( 'hello', $store->signContent( 'hello' ) ) );
	}

	/**
	 * @return void
	 */
	public function testInvalidPath() {
		$store = FileKeystore::createKeystore( 5 );

		$this->assertInstanceOf( Throwable::class, $store );
		$this->assertEquals( 'path is not valid', $store->getMessage() );
	}
}
