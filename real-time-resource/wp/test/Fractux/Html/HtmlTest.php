<?php
namespace Fractux\Html;

final class HtmlTest extends \PHPUnit\Framework\TestCase {
	public function testDoctype(): void {
		$this->assertEquals( "<!DOCTYPE html>\n", Html::doctype() );
		$this->assertInstanceOf( SafeContent::class, Html::doctype() );
	}

	public function testFragment(): void {
		$this->assertEquals( 'Hello <b>there</b>.', (string) Html::fragment( [ 'Hello ', Html::element( 'b', [], 'there' ), '.' ] ) );
		$this->assertEquals( 'Hello <b>there</b>.', (string) fragment( [ 'Hello ', Html::element( 'b', [], 'there' ), '.' ] ) );
		$this->assertEquals( SafeString::escape( 'hello' ), Fragment::from( 'hello' ) );
	}

	public function testFormatting(): void {
		$this->assertEquals(
			<<<HTML
			<div>
				<ul>
					<li>Hello <em>friends</em>.</li>
				</ul>
			</div>
			HTML,
			(string) ( div( [], ul( [], li( [], [ 'Hello ', em( [], 'friends' ), '.' ] ) ) ) )->render( RenderOptions::options( 2 ) )
		);
	}

	public function testFormattingSingleChild(): void {
		$this->assertEquals(
			<<<HTML
			<div>Hello</div>
			HTML,
			(string) ( div( [], 'Hello' ) )->render( RenderOptions::options( 1 ) )
		);
	}

	public function testFormattingInlineFragment(): void {
		$this->assertEquals(
			<<<HTML
			hello <em>everyone</em>.
			HTML,
			(string) fragment( [], 'hello ', em( [], 'everyone' ), '.' )
		);
	}

	public function testFormattingBlockFragment(): void {
		$this->assertEquals(
			<<<HTML

			<header>Header</header>
			<section>Section</section>
			HTML,
			(string) fragment( [],
				header( [], 'Header' ),
				section( [], 'Section' )
			)->render( RenderOptions::options( 2 ) )
		);
	}

	public function testAttributes(): void {
		$this->assertEquals(
			'<hi thing="a" hacker&quot;="/&gt; so sneaky">hi</hi>', (string) element(
				'hi', [
					'thing' => 'a',
					'hacker"' => '/> so sneaky',
				], 'hi'
			)
		);
	}

	public function testSafeContent(): void {
		$this->assertEquals( '<div>My &lt;em&gt;name&lt;/em&gt;</div>', (string) Html::element( 'div', [], 'My <em>name</em>' ) );
		$this->assertEquals( '<div>My <em>name</em></div>', (string) Html::element( 'div', [], Html::unsafe( 'My <em>name</em>' ) ) );
	}

	public function testEscape(): void {
		$this->assertEquals( '&lt;div thing=&quot;other-thing&quot;&gt;Hi&lt;/div&gt;', (string) Html::escape( '<div thing="other-thing">Hi</div>' ) );
	}

	public function testDocument(): void {
		$this->assertEquals(
			<<<HTML
			<!DOCTYPE html>
			<html>Hi</html>
			HTML,
			(string) Html::document( Html::element( 'html', [], 'Hi' ) )
		);
	}

	public function testFragmentOfOne(): void {
		$element = ul( [], li( [], 'hello' ) );
		$fragment = Fragment::from( $element );

		$this->assertTrue( $element === $fragment );
	}

	public function testRenderWhitespace(): void {
		$this->assertEquals(
			'<section>Hello</section>',
			(string) Html::element( 'section', [], 'Hello' )->render( RenderOptions::options( 2 ) )
		);
	}

	public function testHelpers(): void {
		$this->assertEquals(
			<<<HTML
			<!DOCTYPE html>
			<html>Hello</html>
			HTML,
			(string) document( html( [], 'Hello' ) )
		);

		$this->assertEquals( '<title>Document Title</title>', (string) title( [], 'Document Title' ) );
		$this->assertEquals( '<div>DIV Element</div>', (string) div( [], 'DIV Element' ) );
		$this->assertEquals( '<body>Document Body</body>', (string) body( [], 'Document Body' ) );
		$this->assertEquals( '<head>Document Head</head>', (string) head( [], 'Document Head' ) );

		$this->assertEquals(
			'<link type="text/css" href="somewhere"/>',
			(string) link(
				[
					'type' => 'text/css',
					'href' => 'somewhere',
				]
			)
		);

		$this->assertEquals( '<section id="important">hi</section>', (string) section( [ 'id' => 'important' ], 'hi' ) );

		$this->assertEquals( '<em>Definitely safe</em>', (string) unsafe( '<em>Definitely safe</em>' ) );

		$this->assertEquals(
			<<<HTML
			<table>
				<tbody>
					<tr>
						<th><a href="#">Heading</a></th>
						<td>Hello</td>
					</tr>
				</tbody>
			</table>
			HTML,
			(string) table(
				[],
				tbody(
					[], [
						tr(
							[], [
								th( [], a( [ 'href' => '#' ], 'Heading' ) ),
								td( [], 'Hello' ),
							]
						),
					]
				)
			)
		);
	}

}
