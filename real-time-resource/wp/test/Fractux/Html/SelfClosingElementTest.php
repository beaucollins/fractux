<?php
namespace Fractux\Html;

final class SelfClosingElementTest extends \PHPUnit\Framework\TestCase {
	public function testRender(): void {
		$this->assertEquals( '<img src="image.jpg"/>', (string) new SelfClosingElement( 'img', [ 'src' => 'image.jpg' ] ) );
	}
}
