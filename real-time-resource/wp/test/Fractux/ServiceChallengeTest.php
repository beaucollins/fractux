<?php
namespace Fractux;

final class ServiceChallengeTest extends \PHPUnit\Framework\TestCase {

	function testInvalidJSON(): void {
		self::assertInstanceOf( \Throwable::class, ServiceChallenge::decode( '' ) );
		self::assertInstanceOf( \Throwable::class, ServiceChallenge::decode( '{"nonce":""}' ) );
		self::assertInstanceOf( \Throwable::class, ServiceChallenge::decode( '{"nonce":1,"envelope":""}' ) );
		self::assertInstanceOf( \Throwable::class, ServiceChallenge::decode( '{"nonce":"","envelope":9}' ) );
	}

}
