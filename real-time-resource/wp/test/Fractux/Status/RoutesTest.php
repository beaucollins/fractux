<?php
namespace Fractux\Status;

use Fractux\App\{IMatcher, IRequest, IResponse, IUnmatched};

final class RoutesTest extends \PHPUnit\Framework\TestCase {

	public function testGetRoot(): void {
		$response = self::request( 'GET', '/' );
		$this->assertEquals( 200, $response->getStatus() );
	}

	public function testPostRoot(): void {
		$response = self::request( 'POST', '/' );
		$this->assertEquals( 200, $response->getStatus() );
	}

	public function testAdd(): void {
		$response = self::request( 'GET', '/_internal/add/21/1' );
		$this->assertStringContainsString( '<div>Sum: 22</div>', $response->getBody() );
		$this->assertEquals( 200, $response->getStatus() );
	}

	public function testAddFails(): void {
		$response = self::request( 'GET', '/_internal/add/2/a' );
		$this->assertStringContainsString( '<body>Can only add numbers</body>', $response->getBody() );
		$this->assertEquals( 200, $response->getStatus() );
	}

	/**
	 * @param string $method
	 * @param string $path
	 * @return IResponse
	 */
	private static function request( $method, $path ) {
		return \Fractux\Test\Request::request(
			\Fractux\Test\Request::buildRequest( $method, $path ),
			self::build()
		);
	}

	/**
	 * @return IMatcher<IRequest, IResponse>
	 */
	private static function build() {
		return Routes::mount();
	}

}
