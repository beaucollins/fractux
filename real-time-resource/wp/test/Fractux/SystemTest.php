<?php

namespace Fractux;

use Fractux\Dev\{WebhookResource, SodiumSession};
use Fractux\Test\MemoryKeystore;

use Throwable;

final class SystemTest extends \PHPUnit\Framework\TestCase {

	/**
	 * @return void
	 */
	public function testRegisterResource() {
		// Create a Service and Transport, register key
		$system = self::buildSystem();
		self::assertInstanceOf( IRegistration::class, $system->registerResource( self::buildResource() ) );
	}

	/**
	 * @return void
	 */
	public function testAcceptChallenge() {
		$system = self::buildSystem();

		$session = SodiumSession::createServerSession( $system->getClientPublicKey(), $system->getService()->getKeypair() );

		$challenge = self::encode( $session->encrypt( 'congabongo' ) );

		$response = $system->createChallengeResponse( self::buildResource(), $challenge );

		if ( $response instanceof Throwable ) {
			throw $response;
		}

		$encrypted = self::decode( $response );

		if ( $encrypted instanceof Throwable ) {
			throw $encrypted;
		}

		self::assertEquals( 'congabongo', $session->decrypt( $encrypted ) );
	}

	/**
	 * @return IResourceDescriptor
	 */
	private static function buildResource() {
		$resource = WebhookResource::create( 'http://fractux.dev/incoming' );

		if ( $resource instanceof Throwable ) {
			throw $resource;
		}
		return $resource;
	}

	/**
	 * @return System<Test\Service>
	 */
	private static function buildSystem() {
		$system = System::initialize(
			new Test\Service(),
			MemoryKeystore::seed( 'test-time' ),
		);

		if ( $system instanceof Throwable ) {
			throw $system;
		}
		return $system;
	}

	/**
	 * @param IEncryptedData $encrypted
	 * @return string
	 */
	private static function encode( $encrypted ) {
		return json_encode(
			[
				'nonce' => $encrypted->getNonce(),
				'envelope' => $encrypted->getEnvelope(),
			],
			JSON_FORCE_OBJECT
		);
	}

	/**
	 * @param IEncodable $encrypted
	 * @return IEncryptedData|Throwable
	 */
	private static function decode( $encrypted ) {
		return Test\Service::decodeEncrypted( $encrypted->encode() );
	}
}
