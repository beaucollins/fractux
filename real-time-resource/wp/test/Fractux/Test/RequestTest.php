<?php
namespace Fractux\Test;

use Fractux\App\Route;

final class RequestTest extends \PHPUnit\Framework\TestCase {

	public function testRequestUnmatched(): void {
		$this->expectException( \UnexpectedValueException::class );
		Request::request(
			Request::buildRequest( 'GET', '/hello' ),
			Route::neverMatches()
		);
	}
}
