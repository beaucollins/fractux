<?php
namespace Fractux\Test;

use function sodium_bin2hex;
use function sodium_hex2bin;

final class ServiceTest extends \PHPUnit\Framework\TestCase {

	public function testDecodeRegistration(): void {
		$this->assertInstanceOf( \RuntimeException::class, Service::decodeRegistrationRequest( '5' ) );
		$this->assertInstanceOf( \RuntimeException::class, Service::decodeRegistrationRequest( '{}' ) );
		$this->assertInstanceOf(
			\SodiumException::class, Service::decodeRegistrationRequest( json_encode( [
				'publicKey' => 'abc',
			] ) )
		);

		$this->assertInstanceOf(
			\RuntimeException::class, Service::decodeRegistrationRequest( json_encode( [
				'publicKey' => sodium_bin2hex( sodium_hex2bin( 'abc123ab' ) ),
			] ) )
		);

		$this->assertInstanceOf(
			\RuntimeException::class, Service::decodeRegistrationRequest( json_encode( [
				'publicKey' => sodium_bin2hex( sodium_hex2bin( 'abc123ab' ) ),
				'nonce' => 'abcd',
			] ) )
		);

	}

}
