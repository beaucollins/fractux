/**
 * @flow
 */
import type { IncomingMessage } from 'http';

export default function collectBuffers(req: stream$Readable): Promise<Buffer> {
    return new Promise((resolve, reject) => {
        const buffers = [];
        req
            .on('data', (data) => buffers.push(data))
            .on('error', reject)
            .on('end', () => resolve(Buffer.concat(buffers)));
    });
}
