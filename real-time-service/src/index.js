/**
 * @flow
 */
import ws from 'ws';
import { serve, mount, all, get, any, replyWith, replyWithJson, isGet, isPost, requestedPath } from './service';
import type { RouteDetector, RouteHandler } from './service';
import type {IncomingMessage} from 'http';
import type { Duplex } from 'stream';
import Redis from 'ioredis';

import type { Keystore } from './keystore';
import type { Resource } from './repo';

import repo from './repo';
import keystore from './map-keystore';
import readJson from './read-json-message';

const app = all(
    serve(isGet<'/service-descriptor'>('/service-descriptor'), replyWithJson(() => repo.serviceDescriptor())),
    serve(isPost<'/service/register'>('/service/register'), readJson(replyWithJson((req, message) => repo.registerResource(message[1])))),
    serve(isGet<'/other'>('/other'), replyWithJson((req, [method, path, query]) => Buffer.from('wtf'))),
    serve(requestedPath('/path'), replyWithJson((req, [path, query]) => query)),
    serve(get('/path'), replyWith('hello-world')),
    serve(get('/'), redirectTo('https://fractux.collins.pub')),
    serve(any(), replyWith((req) => {
        return Buffer.from(`Not found: ${req.url}`);
    }, 404)),
);

const server = mount(async (req, res) => {
    const time = Date.now();
    res.once('close', () => {
        console.log('%s %s -> %d in %d ms', req.method, req.url, res.statusCode, Date.now() - time);
    });
    try {
        return await app(req, res);
    } catch (error) {
        console.error('Service failed', error);
        const handler = replyWithJson((_, e) => ({error: e.message}), 500);
        handler(req, error, res);
        return true;
    }
});

const sockets = new ws.Server({ noServer: true });

sockets.on('connection', (connection) => {
    connection.on('close', () => {
        console.log('all done');
    });
});

type ResourceResult =
  | {| type: 'known', resource: Resource |}
  | {| type: 'unknown', reason: string |}

type ResourceResolver = (IncomingMessage) => Promise<ResourceResult>;

const resolveResourceOrigin = async (request: IncomingMessage): Promise<ResourceResult> => {
    const origin = request.headers.origin;
    if ( typeof origin !== 'string' ) {
        return unknownResource('Resource specifier not found');
    }
    const resource = await repo.resource.fetch(origin);
    if (!resource) {
        return unknownResource(`Resource not present for ${origin}`);
    }
    return {type: 'known', resource};
}

const authenticate = async (request: IncomingMessage): Promise<ResourceResult> => {
    const results: ResourceResult[] = await Promise.all([resolveResourceOrigin].map(resolver => resolver(request)))
    return results.find(result => result.type === 'known') ?? unknownResource('Resource unknown for request');
}

server.on('upgrade', function(request: IncomingMessage, socket: Duplex, head: Buffer) {
    const close = () => {
        socket.emit('unauthorized')
        socket.destroy();
    }
    authenticate(request).then(
        (result: ResourceResult) => {
            if (result.type === 'known') {
                sockets.handleUpgrade(request, socket, head, (client) => {
                    sockets.emit('connection', client, request);
                })
            } else {
                console.log('Resource not known', result.reason);
                close();
            }
        },
        // Failed auth
        (error) => {
            console.error('Authentication error', error);
            close();
        }
    )
});

server.listen(process.env['PORT'], function() {
    console.warn('Listening', server.address());
});

function redirectTo<T>(location, status = 302): RouteHandler<T> {
    return replyWith(`Redirecting to ${location}`, status, [['Location', location]]);
}

function unknownResource(reason: string): ResourceResult {
    return {type: 'unknown', reason};
}
