/**
 * @flow
 */

export interface Keystore {
	set(serviceName: string, key: Buffer): Promise<void>;
    get(serviceName: string): Promise<?Buffer>;
}