/**
 * @flow
 */
import type { Keystore } from './keystore';

export class MapKeystore implements Keystore {

	map: Map<string, Buffer>

	constructor() {
		this.map = new Map();
	}

	get(serviceName: string): Promise<?Buffer> {
		return new Promise(resolve => {
			resolve(this.map.get(serviceName));
		});
	}

	set(serviceName:string, key:Buffer): Promise<void> {
		return new Promise((resolve) => {
			this.map.set(serviceName, key);
			resolve();
		});
	}
}

const defaultStore: Keystore = new MapKeystore();

export default defaultStore;