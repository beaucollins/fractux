/**
 * @flow
 */
import type { RouteHandler } from './service';
import collectBuffers from './collect-buffers';

type JSONResult = {[string]: mixed, ...} | Array<{[string]: mixed}>;
export default function readJson<T>(jsonHandler: RouteHandler<[T, JSONResult]>): RouteHandler<T> {
    return function(req, detected, response) {
        return collectBuffers(req)
            .then(String)
            .then(JSON.parse)
            .then(json => jsonHandler(req, [detected, json], response))
    }
}

