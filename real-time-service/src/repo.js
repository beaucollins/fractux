/**
 * @flow
 */
import sodium from 'sodium-native';
import { createReadStream, createWriteStream } from 'fs';
import collectBuffers from './collect-buffers';
import { request } from 'http';
import { URL } from 'url';
import { Readable } from 'stream';

export type Resource = {|

|};

type ServiceDescriptor = {|
	+publicKey: string
|};

type KeyPair = {|
	+publicKey: Buffer,
	+secretKey: Buffer
|};

const repo = {
	resource: {
		fetch: (origin: string): Promise<?Resource> => {
			return Promise.resolve(null);
		}
	},
	async serviceDescriptor() {
		const publicKey = await this.publicKey();
		return {
			publicKey: publicKey.toString('hex'),
		};
	},

	async registerResource( registrationRequest: any ) {
		const publicKey = Buffer.from( registrationRequest.publicKey, 'hex' );

		const keyPair: KeyPair = await this.keyPair();

		const rx = Buffer.alloc( sodium.crypto_kx_SESSIONKEYBYTES );
		const tx = Buffer.alloc( sodium.crypto_kx_SESSIONKEYBYTES );
		sodium.crypto_kx_server_session_keys( rx, tx, keyPair.publicKey, keyPair.secretKey, publicKey );

		const decrypted = decrypt(registrationRequest, rx);

		const registration = JSON.parse(decrypted.toString('utf8'));

		// make a webhook request to complete the registration
		const key = Buffer.alloc( 16 );
		sodium.randombytes_buf(key);

		const challenge = Buffer.from(
			JSON.stringify({api: '1.0.0', challenge: 'hello', key: key.toString('hex')}),
			'utf8'
		);
		const encrypted = Buffer.alloc(challenge.length + sodium.crypto_secretbox_MACBYTES);
		const nonce = Buffer.alloc(sodium.crypto_secretbox_NONCEBYTES);

		sodium.randombytes_buf(nonce);
		sodium.crypto_secretbox_easy(encrypted, challenge, nonce, tx);

		const body = JSON.stringify({
			envelope: encrypted.toString('hex'),
			nonce: nonce.toString('hex'),
		});

		const response = await sendRequest(registration.url, (Readable: any).from([body]));

		const challengeResponse = JSON.parse(decrypt(JSON.parse(response.toString('utf8')), rx).toString('utf8'));

		if (challengeResponse.key !== key.toString('hex')) {
			throw new Error('Invalid key');
		}

		// Store the client key for this resource.
		return { status: 'registered', message: registration };
	},

	generateKeyPair(): KeyPair {
		console.log('generating keypair');
		const publicKey = Buffer.alloc(sodium.crypto_kx_PUBLICKEYBYTES);
		const secretKey = Buffer.alloc(sodium.crypto_kx_SECRETKEYBYTES);
		sodium.crypto_kx_keypair(publicKey, secretKey);
		return {publicKey, secretKey};
	},

	async readKeyPair(): Promise<KeyPair> {
		console.log('reading key from disk');
		const buffer = await collectBuffers(createReadStream('/var/data/app/keypair.json'));
		if (!buffer) {
			throw new Error('no keypair');
		}

		const json = JSON.parse(buffer.toString('utf8'));

		return {
			publicKey: Buffer.from(json.publicKey, 'hex'),
			secretKey: Buffer.from(json.secretKey, 'hex'),
		};
	},

	async keyPair(): Promise<KeyPair> {
		try {
			return await this.readKeyPair();
		} catch (error) {
			console.error('failed to read keypair', error);
		}

		try {
			const keyPair = this.generateKeyPair();
			await writeKeyPair(keyPair, '/var/data/app/keypair.json');
			return keyPair;
		} catch (error) {
			throw new Error(`Failed to generate keypair: ${error.message}`);
		}

	},

	async publicKey(): Promise<Buffer> {
		return (await this.keyPair()).publicKey;
	}
}

function writeKeyPair(keyPair: KeyPair, path: string): Promise<void> {
	return new Promise((resolve, reject) => {
		const file = createWriteStream(path);
		file.write(
			JSON.stringify({
				publicKey: keyPair.publicKey.toString('hex'),
				secretKey: keyPair.secretKey.toString('hex'),
			}),
			(error) => {
				if (error) {
					reject(error);
				}
			}
		);
		file.on('error', reject);
		file.end(() => {
			resolve();
		})
	})

}

function sendRequest(registrationUrl: string, stream?: ReadableStream): Promise<Buffer> {
	return new Promise( (resolve, reject) => {
		const url = new URL(registrationUrl);
		const req = request({
			hostname: url.hostname,
			port: url.port ? parseInt(url.port) : 80,
			path: url.pathname + url.search,
			method: stream ? 'POST' : 'GET',
			headers: { 'accept': 'application/json' }
		});

		req.on('error', reject)
		req.on('response', (response) => {
			collectBuffers(response).then((buffer) => {
				if (response.statusCode === 200) {
					return resolve(buffer);
				}
				return reject(new Error(`Request failed with ${response.statusCode}: ${url.hostname}${url.pathname}`))
			}, reject);
		});
		stream
			? (stream: any).pipe((req: any))
			: req.end()
	});
}

function decrypt(message: any, secret: Buffer) {
	const envelope = Buffer.from( message.envelope, 'hex' );
	const nonce = Buffer.from( message.nonce, 'hex' );

	const decrypted = Buffer.alloc( envelope.length - sodium.crypto_secretbox_MACBYTES );
	sodium.crypto_secretbox_open_easy( decrypted, envelope, nonce, secret );
	return decrypted;
}

export { repo as default };