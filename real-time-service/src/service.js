/**
 * @flow
 */
import type { IncomingMessage, ServerResponse } from 'http';
import { createServer } from 'http';
import { parse as parseQuery } from 'querystring';

export type Result<T> =
 | {| type: 'match', value: T |}
 | {| type: 'miss' |}

export type RouteDetector<T> = (IncomingMessage) => Result<T>
export type RouteHandler<T> = (IncomingMessage, T, ServerResponse) => void|Promise<void>
export type Service = (IncomingMessage, ServerResponse) => boolean|Promise<boolean>;

export function serve<T>(detector: RouteDetector<T>, handler: RouteHandler<T>): Service {
    return async function(request, response) {
        const detected: Result<T> = detector(request);
        if (detected.type === 'miss') {
            return false;
        }
        const value: T = detected.value;
        await handler(request, value, response);
        return true;
    }
}

const MISS = { type: 'miss' };
export function missed<T>(): Result<T> {
    return MISS;
}

export function matched<T>(value: T): Result<T> {
    return { type: 'match', value };
}

export function get<T: string>(path: T): RouteDetector<T> {
    return function(req) {
        if (req.method === 'GET' && req.url === path) {
            return matched(path);
        }
        return missed();
    }
}

type Header = [string, string | Array<string>];
type Headers = Array<Header>;

export function replyWith<T>(content: string | (IncomingMessage, T) => (Buffer|Promise<Buffer>), statusCode: number = 200, headers: Headers = []): RouteHandler<T> {
    const handler: (IncomingMessage, T) => (Buffer|Promise<Buffer>) =
        typeof content === 'string'
            ? async () => Buffer.from(await content, 'utf-8')
            : content;

    return async function(req, detected, res) {
        console.log('replyig with');
        const buffer = await handler(req, detected);
        res.statusCode = statusCode;
        res.setHeader('content-type', 'text/plain');
        res.setHeader('content-length', String(buffer.length));

        headers.forEach(([name, value]) => {
            const values = Array.isArray(value) ? value : [value];
            for (const headerValue of values) {
                res.setHeader(name, headerValue);
            }
        });

        res.write(buffer);
        res.end();
    }
}

export function replyWithJson<T>(content: any | Promise<any> | (IncomingMessage, T) => any|Promise<any>, statusCode: number = 200, headers: Headers = []): RouteHandler<T> {
    const handler: (IncomingMessage, T) => any =
        typeof content === 'function'
        ? async (message, context) => Buffer.from(JSON.stringify(await content(message, context)), 'utf-8')
        : async () => Buffer.from(JSON.stringify(await content), 'utf-8')

    return replyWith<T>(handler, statusCode, headers);
}

export function all(...services: Array<Service>): Service {
    return async function (req, res) {
        for(const service of services) {
            if (await service(req, res)) {
                return true;
            }
        }
        return false;
    }
}

export function any(): RouteDetector<true> {
    return () => matched(true);
}

export function mount(app: Service) {
    const server = createServer(function(req, res) {
        app(req, res)
    });
    return server;
}

export function requestedPath<P: string>(path: P): RouteDetector<[P, {[string]: string,...}]> {
    return function(req) {
        const url = req.url;
        const indicator = req.url.indexOf('?');
        const requestedPath = indicator > -1 ? url.slice(0, indicator) : url;

        if (requestedPath !== path) {
            return missed();
        }

        const query = indicator === -1 ? null : url.slice(indicator + 1);
        return matched([path, query == null ? {} : parseQuery(query)]);
    }
}


function both<A, B>(a: RouteDetector<A>, b: RouteDetector<B>): RouteDetector<[A, B]> {
    return function (req): Result<[A, B]> {
        const detectedA = a(req);
        const detectedB = b(req);

        if ( detectedA.type === 'miss' || detectedB.type === 'miss') {
            return missed();
        }

        return matched([detectedA.value, detectedB.value]);
    }
}

function methodIs<T: 'GET' | 'POST'>(method: T): RouteDetector<T> {
    return function(req) {
        if (req.method === method) {
            return matched(method);
        }
        return missed();
    }
}

function isMiss<T>(result: Result<T>): boolean %checks {
    return result.type === 'miss';
}

function mapDetector<A, B>(detector: RouteDetector<A>, map: A => B): RouteDetector<B> {
    return function(req) {
        const detected = detector(req);
        if (detected.type === 'miss') {
            return detected;
        }
        return matched(map(detected.value));
    }
}

function flattenDetectors<A, B, C>(a: RouteDetector<A>, b: RouteDetector<[B, C]>): RouteDetector<[A, B, C]> {
    return mapDetector(both(a, b), ([a, [b, c]]) => [a, b, c]);
}

type Method = | 'GET' | 'POST';
type Query = {[string]: string, ...};

function route<T: Method, P: string>(method: T, path: P): RouteDetector<[T, P, Query]> {
    return flattenDetectors<T, P, Query>(methodIs<T>(method), requestedPath<P>(path));
}

export const isGet = <P: string>(path: P): RouteDetector<['GET', P, Query]> => route<'GET', P>('GET', path);
export const isPost = <P: string>(path: P): RouteDetector<['POST', P, Query]> => route<'POST', P>('POST', path);
